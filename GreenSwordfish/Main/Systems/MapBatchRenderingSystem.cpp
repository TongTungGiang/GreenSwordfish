#include "MapBatchRenderingSystem.h"
#include "Components/SpriteComponent.h"
#include "Game.h"

namespace GS
{
static SDL_Rect FromRect(Rect rect)
{
	return { static_cast<int>(rect.x), static_cast<int>(rect.y), static_cast<int>(rect.width), static_cast<int>(rect.height) };
}

void MapBatchRenderingSystem::Init(Registry& registry)
{
	auto batchRenderingQuery = registry.view<BatchRenderingSpriteComponent, SpriteComponent>();
	if (batchRenderingQuery.empty())
		return;

	Rect screenSize(0, 0, WIDTH, HEIGHT);
	SDL_Renderer* renderer = Game::Get()->GetGlobalSDLRenderer();
	m_BatchResultTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, screenSize.width, screenSize.height);

	SDL_SetRenderTarget(renderer, m_BatchResultTexture);

	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);

	for (auto it = std::make_reverse_iterator(batchRenderingQuery.end());
		it != std::make_reverse_iterator(batchRenderingQuery.begin());
		it++)
	{
		Entity entity = *it;
		SpriteComponent& sprite = batchRenderingQuery.get<SpriteComponent>(entity);

		SDL_Rect render = FromRect(sprite.renderRect);
		SDL_Rect clip = FromRect(sprite.GetClipRect());
		SDL_Texture* texture = sprite.GetTexture();

		SDL_RenderCopy(renderer, texture, &clip, &render);
	}

	registry.destroy(batchRenderingQuery.begin(), batchRenderingQuery.end());

	SDL_SetRenderTarget(renderer, NULL);

	auto batchResultSpriteEntity = registry.create();
	registry.emplace<SpriteComponent>(batchResultSpriteEntity,
		SpriteComponent(screenSize, m_BatchResultTexture, screenSize));
	registry.emplace<BakedSpriteComponent>(batchResultSpriteEntity, BakedSpriteComponent());
}

void MapBatchRenderingSystem::Exit(Registry& registry)
{
	SDL_DestroyTexture(m_BatchResultTexture);
	m_BatchResultTexture = NULL;
}

}