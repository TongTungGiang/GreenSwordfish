#pragma once

#include "SystemBase.h"

namespace GS
{
class MapBatchRenderingSystem : public SystemBase
{
public:
	MapBatchRenderingSystem(GameWorld* world) : SystemBase(world), m_BatchResultTexture(NULL) { }
	virtual void Update(Registry& registry, float deltaTime) override { }
	virtual void Init(Registry& registry) override;
	virtual void Exit(Registry& registry) override;

private:
	SDL_Texture* m_BatchResultTexture;
};

}
