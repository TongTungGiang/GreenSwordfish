#include "ClickSystem.h"
#include "Events/ClickEvent.h"

#include "Components/ClickableComponent.h"
#include "Components/SpriteComponent.h"

namespace GS
{


void ClickSystem::Update(Registry& registry, float deltaTime) {	PROFILE_SCOPE(); }

void ClickSystem::Init(Registry& registry)
{
	m_World->BindEvent<ClickEvent, ClickSystem, &ClickSystem::ProcessClick>(this);
}

void ClickSystem::Exit(Registry& registry)
{
	m_World->UnbindEvent<ClickEvent>(this);
}

void ClickSystem::ProcessClick(const ClickEvent& ev)
{
	Debug::Log("Just clicked at " + std::to_string(ev.mousePosition.x) + " " + std::to_string(ev.mousePosition.y));

	Registry& registry = m_World->GetRegistry();

	auto view = registry.view<SpriteComponent, ClickableComponent>();
	Debug::Log("Clickable count = " + std::to_string(view.size()));
	for (auto entity : view)
	{
		const SpriteComponent& sprite = registry.get<SpriteComponent>(entity);
		if (Math::IsInRect(sprite.renderRect, ev.mousePosition))
		{
			Debug::Log("Clicked " + std::to_string((unsigned int)entity));
			const ClickableComponent& click = registry.get<ClickableComponent>(entity);
			if (click.onClick != nullptr)
				click.onClick(registry, entity);
			break;
		}
	}
}

}
