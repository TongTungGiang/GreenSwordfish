#include "TextRenderingSystem.h"
#include "Components/TextComponent.h"

namespace GS
{
void TextRenderingSystem::Update(Registry& registry)
{
	PROFILE_SCOPE();

	SDL_Renderer* sdlRenderer = m_Renderer->GetSdlRenderer();
	TTF_Font* defaultFont = m_Renderer->GetResourceManager()->GetFont("Arial");
	registry.view<TextComponent>().each(
		[&registry, sdlRenderer, defaultFont](auto entity, TextComponent& text)
		{
			SDL_Surface* textSurface = TTF_RenderText_Solid(defaultFont, text.value.c_str(), SDL_Color{ text.color.r, text.color.g, text.color.b, text.color.a });
			SDL_Texture* textTexture = SDL_CreateTextureFromSurface(sdlRenderer, textSurface);
			SDL_FreeSurface(textSurface);

			float specifiedHeight = text.renderRect.height;
			int queriedWidth, queriedHeight;
			SDL_QueryTexture(textTexture, NULL, NULL, &queriedWidth, &queriedHeight);

			float heightWidthRatio = (float)queriedHeight / (float)queriedWidth;
			int normalisedHeight = (int)specifiedHeight;
			int normalisedWidth = (int)((float)normalisedHeight / heightWidthRatio);

			SDL_Rect renderRect = text.renderRect.ToSDLRect();
			renderRect.w = normalisedWidth;
			renderRect.h = normalisedHeight;
			SDL_RenderCopy(sdlRenderer, textTexture, NULL, &renderRect);

			SDL_DestroyTexture(textTexture);
		}
	);
}
}

