#include "SpriteRenderingSystem.h"
#include "Components/SpriteComponent.h"
#include <Components/UISortLayerComponent.h>

namespace GS
{

static SDL_Rect FromRect(Rect rect)
{
	return { static_cast<int>(rect.x), static_cast<int>(rect.y), static_cast<int>(rect.width), static_cast<int>(rect.height) };
}

static void RenderSprite(const SpriteComponent& sprite, SDL_Renderer* renderer)
{
	if (sprite.GetTexture() == NULL)
		return;

	SDL_Rect render = FromRect(sprite.renderRect);
	SDL_Rect clip = FromRect(sprite.GetClipRect());
	SDL_Texture* texture = sprite.GetTexture();

	SDL_RenderCopy(renderer, texture, &clip, &render);

#if false
	SDL_SetRenderDrawColor(sdlRenderer, DEBUG_COLOR.r, DEBUG_COLOR.g, DEBUG_COLOR.b, DEBUG_COLOR.a);
	SDL_RenderDrawLine(sdlRenderer,
		sprite.renderRect.x,
		sprite.renderRect.y,
		sprite.renderRect.x + sprite.renderRect.width,
		sprite.renderRect.y);
	SDL_RenderDrawLine(sdlRenderer,
		sprite.renderRect.x,
		sprite.renderRect.y,
		sprite.renderRect.x,
		sprite.renderRect.y + sprite.renderRect.height);
	SDL_RenderDrawLine(sdlRenderer,
		sprite.renderRect.x + sprite.renderRect.width,
		sprite.renderRect.y + sprite.renderRect.height,
		sprite.renderRect.x + sprite.renderRect.width,
		sprite.renderRect.y);
	SDL_RenderDrawLine(sdlRenderer,
		sprite.renderRect.x + sprite.renderRect.width,
		sprite.renderRect.y + sprite.renderRect.height,
		sprite.renderRect.x,
		sprite.renderRect.y + sprite.renderRect.height);
	SDL_SetRenderDrawColor(sdlRenderer, DEFAULT_SDL_COLOR.r, DEFAULT_SDL_COLOR.g, DEFAULT_SDL_COLOR.b, DEFAULT_SDL_COLOR.a);

#endif
}

void SpriteRenderingSystem::Update(Registry& registry)
{
	PROFILE_SCOPE();

	SDL_Renderer* renderer = m_Renderer->GetSdlRenderer();

	// Baked texture rendering
	registry.view<SpriteComponent, BakedSpriteComponent>().each(
		[renderer](const SpriteComponent& sprite)
		{
			RenderSprite(sprite, renderer);
		}
	);

	// World rendering
	registry.view<SpriteComponent>(entt::exclude<UISortLayerComponent, BakedSpriteComponent>).each(
		[renderer](const SpriteComponent& sprite)
		{
			RenderSprite(sprite, renderer);
		}
	);

	// UI/HUD rendering
	registry.view<SpriteComponent, UISortLayerComponent>().each(
		[renderer](const SpriteComponent& sprite, const UISortLayerComponent& uiSortLayer)
		{
			RenderSprite(sprite, renderer);
		}
	);
}
}
