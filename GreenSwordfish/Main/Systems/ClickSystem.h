#pragma once

#include "SystemBase.h"

namespace GS
{

struct ClickEvent;

class ClickSystem : public SystemBase
{
public:
	ClickSystem(GameWorld* world) : SystemBase(world) { }
	
	virtual void Update(Registry& registry, float deltaTime) override;
	virtual void Init(Registry& registry) override;
	virtual void Exit(Registry& registry) override;

private:
	void ProcessClick(const ClickEvent& ev);
};

}
