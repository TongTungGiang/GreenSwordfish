#include "UpdateMapSpriteSystem.h"

#include "Components/TileComponent.h"
#include "Components/SpriteComponent.h"
#include "Components/ArrowComponent.h"

#include "Data/MapData.h"

namespace GS
{

static void UpdateTileTypeSprite(const MapTileArray& mapArrayData,
	TileComponent& tile,
	TileTypeComponent& tileType,
	SpriteComponent& sprite)
{
	int linearTileIndex = GetTileIndex(tile.x, tile.y);
	if (!IsTileCoordinateValid(mapArrayData, tile.x, tile.y))
		return;

	tileType.value = mapArrayData.at(linearTileIndex).type;

	Vector2 size;

	Vector2 world = Math::TileCoordinateToWorld(tile.x, tile.y);
	Vector2 iso = Math::WorldToIso(world);
	if (tileType.value == TileType::Blank)
	{
		size = Vector2(TILE_SPRITE_WIDTH_BLANK, TILE_SPRITE_HEIGHT_BLANK);
	}
	else if (tileType.value >= TileType::Building1 && tileType.value <= TileType::Building4)
	{
		size = Vector2(BUILDING_SPRITE_WIDTH, BUILDING_SPRITE_HEIGHT);
		iso.y -= (BUILDING_SPRITE_HEIGHT - TILE_SPRITE_HEIGHT_BLANK);
	}
	else
	{
		size = Vector2(TILE_SPRITE_WIDTH, TILE_SPRITE_HEIGHT);
	}

	sprite.renderRect = Rect(iso.x, iso.y, size.x, size.y);
	sprite.SetTexture(g_TileSprites[tileType.value]);
}

static void UpdateArrowSprite(const MapTileArray& mapArrayData,
	const TileComponent tile,
	ArrowComponent& arrow,
	SpriteComponent& sprite
)
{
	int linearTileIndex = GetTileIndex(tile.x, tile.y);
	if (!IsTileCoordinateValid(mapArrayData, tile.x, tile.y))
		return;

	arrow.direction = mapArrayData.at(linearTileIndex).direction;
	sprite.SetTexture(GetArrowSpriteID(arrow.direction, mapArrayData.at(linearTileIndex).type));
}

void UpdateMapSpriteSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();
	MapTileArray filteredMapArrayData;

	{
		PROFILE_SCOPE("FilterDirtyMapTiles");
		MapTileArray& mapArrayData = registry.ctx<MapTileArray>();
		for (auto it = mapArrayData.begin(); it != mapArrayData.end(); it++)
		{
			if (it->second.isDirty)
			{
				filteredMapArrayData.emplace(std::make_pair(it->first, it->second));
				it->second.isDirty = false;
			}
		}
	}

	if (filteredMapArrayData.size() == 0)
		return;

	{
		PROFILE_SCOPE("UpdateTileTypeSprite_All");
		auto tileTypeGroup = registry.group<TileTypeComponent>(entt::get<TileComponent, SpriteComponent>);
		std::for_each(std::execution::seq,
			tileTypeGroup.begin(),
			tileTypeGroup.end(),
			[&tileTypeGroup, &filteredMapArrayData](const auto entity)
			{
				TileComponent& tile = tileTypeGroup.get<TileComponent>(entity);
				TileTypeComponent& tileType = tileTypeGroup.get<TileTypeComponent>(entity);
				SpriteComponent& sprite = tileTypeGroup.get<SpriteComponent>(entity);
				UpdateTileTypeSprite(filteredMapArrayData, tile, tileType, sprite);
			}
		);
	}

	{
		PROFILE_SCOPE("UpdateArrowSprite");
		auto arrowGroup = registry.group<ArrowComponent>(entt::get<TileComponent, SpriteComponent>);
		std::for_each(std::execution::seq,
			arrowGroup.begin(),
			arrowGroup.end(),
			[&arrowGroup, &filteredMapArrayData](const auto entity)
			{
				TileComponent& tile = arrowGroup.get<TileComponent>(entity);
				ArrowComponent& arrow = arrowGroup.get<ArrowComponent>(entity);
				SpriteComponent& sprite = arrowGroup.get<SpriteComponent>(entity);
				UpdateArrowSprite(filteredMapArrayData, tile, arrow, sprite);
			}
		);
	}
}

}

