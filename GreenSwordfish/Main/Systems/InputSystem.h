#pragma once

#include "SystemBase.h"

namespace GS
{

class InputSystem : public SystemBase
{
public:
	InputSystem(class GameWorld* world) : SystemBase(world) {}
	virtual void Update(Registry& registry, float deltaTime) override;
	virtual void Init(Registry& registry) override;

public:
	static void HandleSDLEvent(Registry& registry, SDL_Event& event);

private:
	static void RegisterMouseButtonDown(Registry& registry, int button);
	static void RegisterMouseButtonUp(Registry& registry, int button);
	static void RegisterMouseMotion(Registry& registry, int mouseX, int mouseY);
	static void HandleNewCharacter(Registry& registry, char* c);
	static void HandleBackspace(Registry& registry);
	static void HandleCopy(Registry& registry);
	static void HandlePaste(Registry& registry);

public:
	static Entity GetInputEntity(Registry& registry);
};

}

