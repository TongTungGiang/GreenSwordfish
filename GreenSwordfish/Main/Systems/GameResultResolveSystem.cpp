#include "GameResultResolveSystem.h"

#include "Events/TimesUpEvent.h"
#include "Data/MapData.h"
#include "Data/GameScoreData.h"
#include "Game.h"
#include "WorldManager.h"

#include "MoveForwardSystem.h"
#include "GeometrySortSystem.h"
#include "CarDestroySystem.h"
#include "CarMovementSystem.h"
#include "CarSpawnSystem.h"
#include "TimerSystem.h"

using namespace sdlgui;

namespace GS
{

void GameResultResolveSystem::Init(Registry& registry)
{
	m_World->BindEvent<TimesUpEvent, GameResultResolveSystem, &GameResultResolveSystem::OnTimesUp>(this);
}

void GameResultResolveSystem::Exit(Registry& registry)
{
	m_World->UnbindEvent<TimesUpEvent>(this);
}

void GameResultResolveSystem::OnTimesUp(const TimesUpEvent&)
{
	// Game logic setup
	m_World->UnbindEvent<TimesUpEvent>(this);

	m_World->EnableSystem<TimerSystem>(false);
	m_World->EnableSystem<CarSpawnSystem>(false);
	m_World->EnableSystem<CarMovementSystem>(false);
	m_World->EnableSystem<CarDestroySystem>(false);
	m_World->EnableSystem<GeometrySortSystem>(false);
	m_World->EnableSystem<MoveForwardSystem>(false);

	// Result popup setup
	auto& window = Game::Get()->GetGUI()->GetGuiWindow();
	Window& resultWindow = window.wdg<Window>("Game Over!");
	Theme* resultWindowTheme = new Theme(Game::Get()->GetGlobalSDLRenderer());
	resultWindow.withFixedSize(window.fixedSize() / 2);
	resultWindow.setTheme(resultWindowTheme);

	auto* layout = new GridLayout(Orientation::Horizontal, 1, Alignment::Middle, 20, 25);
	layout->setColAlignment({ Alignment::Middle });
	resultWindow.setLayout(layout);

	const MapGameplayInfo& info = m_World->GetRegistry().ctx<MapGameplayInfo>();
	int requiredScore = info.targetScore;

	{
		auto& label = resultWindow.label("Target score: " + std::to_string(requiredScore));
		label.setFontSize(18);
	}

	const ScoreData& score = m_World->GetRegistry().ctx<ScoreData>();

	int currentScore = score.currentScore;
	{
		auto& label = resultWindow.label("Current score: " + std::to_string(currentScore));
		label.setFontSize(18);
	}

	{
		std::string result;
		sdlgui::Color textColor;
		if (currentScore >= requiredScore)
		{
			result = "YOU WIN!";
			textColor = { 0.0f, 0.9f, 0.1f, 1.0f };
		}
		else
		{
			result = "YOU LOSE!";
			textColor = { 0.85f, 0.1f, 0.1f, 1.0f };
		}

		auto& label = resultWindow.label(result);
		label.setFontSize(30);
		label.setColor(textColor);
	}

	{
		resultWindow.button("Exit").withCallback([]()
			{
				Game::Get()->GetWorldManager()->LoadWorldSafe(WorldName::Menu);
			}
		);
	}

	Game::Get()->GetGUI()->performLayout();
	resultWindow.center();
}

}