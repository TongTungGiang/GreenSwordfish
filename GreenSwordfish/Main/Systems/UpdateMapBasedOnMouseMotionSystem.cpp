#include "UpdateMapBasedOnMouseMotionSystem.h"
#include "Events/ClickEvent.h"

#include "Components/SpriteComponent.h"
#include "Components/TileComponent.h"

#include "Components/IsometricClickableComponent.h"

#include "Data/DragData.h"
#include "Data/MapData.h"
#include <random>

namespace GS
{

void UpdateMapBasedOnMouseMotionSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();
	bool mapDirty = DrawTileLine(registry);
	if (mapDirty)
	{
		SetupSpecialTiles(registry);
	}
}

bool UpdateMapBasedOnMouseMotionSystem::DrawTileLine(Registry& registry)
{
	DragData* dragDataPtr = registry.try_ctx<DragData>();
	if (dragDataPtr == nullptr)
	{
		return false;
	}

	MapTileArray& mapData = registry.ctx<MapTileArray>();

	Vector2 startTile = Math::IsoToTileCoordinate(dragDataPtr->startPosition);
	Vector2 endTile = Math::IsoToTileCoordinate(dragDataPtr->currentPosition);

	// Only allows drawing in straight lines (for now)
	bool sameX = startTile.x == endTile.x;
	bool sameY = startTile.y == endTile.y;
	if (sameX == sameY)
	{
		return false;
	}

	// Figure out the coordinate that the mouse swept
	int maxX, maxY, minX, minY;
	maxX = minX = (int)startTile.x;
	maxY = minY = (int)startTile.y;

	TileType defaultTileType;
	if (startTile.x == endTile.x)
	{
		maxY = (int)(startTile.y > endTile.y ? startTile.y : endTile.y);
		minY = (int)(startTile.y < endTile.y ? startTile.y : endTile.y);
		defaultTileType = TileType::VerticalRoad;
	}
	else if (startTile.y == endTile.y)
	{
		maxX = (int)(startTile.x > endTile.x ? startTile.x : endTile.x);
		minX = (int)(startTile.x < endTile.x ? startTile.x : endTile.x);
		defaultTileType = TileType::HorizontalRoad;
	}

	// Put raw states into tiles.
	Vector2 direction = Math::Normalize(endTile - startTile);
	for (int x = minX; x <= maxX; x++)
	{
		for (int y = minY; y <= maxY; y++)
		{
			int currentTile = GetTileIndex(x, y);
			MapTileInfo tileInfo;
			tileInfo.type = defaultTileType;
			tileInfo.direction = direction;
			tileInfo.isDirty = true;
			mapData[currentTile] = tileInfo;
		}
	}

	return true;
}

void UpdateMapBasedOnMouseMotionSystem::SetupSpecialTiles(Registry& registry)
{
	MapTileArray& mapData = registry.ctx<MapTileArray>();

	for (auto it = mapData.begin(); it != mapData.end(); it++)
	{
		int currentIndex = it->first;
		Vector2 coordinate = GetTileCoordinate(currentIndex);

		bool hasEastNeighbor = IsTileCoordinateValid(mapData, coordinate + Vector2::east) ?
			mapData[GetTileIndex(coordinate + Vector2::east)].type != TileType::Blank :
			false;
		bool hasWestNeighbor = IsTileCoordinateValid(mapData, coordinate + Vector2::west) ?
			mapData[GetTileIndex(coordinate + Vector2::west)].type != TileType::Blank :
			false;
		bool hasNorthNeighbor = IsTileCoordinateValid(mapData, coordinate + Vector2::north) ?
			mapData[GetTileIndex(coordinate + Vector2::north)].type != TileType::Blank :
			false;
		bool hasSouthNeighbor = IsTileCoordinateValid(mapData, coordinate + Vector2::south) ?
			mapData[GetTileIndex(coordinate + Vector2::south)].type != TileType::Blank :
			false;

		// Cross junction
		if (hasEastNeighbor && hasWestNeighbor && hasSouthNeighbor && hasNorthNeighbor)
			mapData[currentIndex].type = TileType::JunctionX;

		// T junction
		else if (hasEastNeighbor && hasWestNeighbor && hasSouthNeighbor)
			mapData[currentIndex].type = TileType::JunctionT_S;
		else if (hasEastNeighbor && hasWestNeighbor && hasNorthNeighbor)
			mapData[currentIndex].type = TileType::JunctionT_N;
		else if (hasEastNeighbor && hasSouthNeighbor && hasNorthNeighbor)
			mapData[currentIndex].type = TileType::JunctionT_E;
		else if (hasWestNeighbor && hasSouthNeighbor && hasNorthNeighbor)
			mapData[currentIndex].type = TileType::JunctionT_W;

		// L junction
		else if (hasEastNeighbor && hasNorthNeighbor)
			mapData[currentIndex].type = TileType::JunctionL_NE;
		else if (hasEastNeighbor && hasSouthNeighbor)
			mapData[currentIndex].type = TileType::JunctionL_SE;
		else if (hasWestNeighbor && hasNorthNeighbor)
			mapData[currentIndex].type = TileType::JunctionL_NW;
		else if (hasWestNeighbor && hasSouthNeighbor)
			mapData[currentIndex].type = TileType::JunctionL_SW;

		// Ends
		else if (hasEastNeighbor && !hasWestNeighbor)
		{
			if (Math::Dot(mapData[currentIndex].direction, Vector2::east) > 0)
				mapData[currentIndex].type = TileType::StartPoint;
			else
				mapData[currentIndex].type = TileType::Building1;
		}
		else if (hasWestNeighbor && !hasEastNeighbor)
		{
			if (Math::Dot(mapData[currentIndex].direction, Vector2::west) > 0)
				mapData[currentIndex].type = TileType::StartPoint;
			else
				mapData[currentIndex].type = TileType::Building1;
		}
		else if (hasNorthNeighbor && !hasSouthNeighbor)
		{
			if (Math::Dot(mapData[currentIndex].direction, Vector2::north) > 0)
				mapData[currentIndex].type = TileType::StartPoint;
			else
				mapData[currentIndex].type = TileType::Building1;
		}
		else if (hasSouthNeighbor && !hasNorthNeighbor)
		{
			if (Math::Dot(mapData[currentIndex].direction, Vector2::south) > 0)
				mapData[currentIndex].type = TileType::StartPoint;
			else
				mapData[currentIndex].type = TileType::Building1;
		}

		mapData[currentIndex].isDirty = true;
	}
}

}

