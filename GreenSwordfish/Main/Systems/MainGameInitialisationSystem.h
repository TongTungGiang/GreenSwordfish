#pragma once

#include "SystemBase.h"


namespace GS
{
class MainGameInitialisationSystem : public SystemBase
{
public:
	MainGameInitialisationSystem(GameWorld* world) : SystemBase(world) { }
	virtual void Init(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override { }
	virtual void Exit(Registry& registry) override;

private:
	void InitUI(Registry& registry);
	void InitMap(Registry& registry);
};
};

