#include "JunctionClickSystem.h"
#include "Events/ClickEvent.h"
#include "Data/MapData.h"

namespace GS
{

JunctionClickSystem::JunctionClickSystem(GameWorld* world) : SystemBase(world)
{
	m_SwitchableDirections = { Vector2::east, Vector2::north, Vector2::west, Vector2::south };
}

void JunctionClickSystem::Init(Registry& registry)
{
	m_World->BindEvent<ClickEvent, JunctionClickSystem, &JunctionClickSystem::ProcessClick>(this);
}

void JunctionClickSystem::Exit(Registry& registry)
{
	m_World->UnbindEvent<ClickEvent>(this);
}


void JunctionClickSystem::ProcessClick(const ClickEvent& ev)
{
	Vector2 clickedTile = Math::IsoToTileCoordinate(ev.mousePosition);
	int clickedTileIndex = GetTileIndex(clickedTile);
	MapTileArray& mapData = m_World->GetRegistry().ctx<MapTileArray>();
	if (mapData.find(clickedTileIndex) == mapData.end())
	{
		return;
	}


	TileType clickedTileType = mapData.at(clickedTileIndex).type;
	bool isJunction = clickedTileType == TileType::JunctionX ||
		clickedTileType == TileType::JunctionT_E ||
		clickedTileType == TileType::JunctionT_N ||
		clickedTileType == TileType::JunctionT_S ||
		clickedTileType == TileType::JunctionT_W;
	if (!isJunction)
	{
		return;
	}

	Vector2 clickedTileDirection = mapData.at(clickedTileIndex).direction;
	auto it = std::find(m_SwitchableDirections.begin(), m_SwitchableDirections.end(), clickedTileDirection);

	for (int iteration = 1; iteration <= 4; iteration++)
	{
		it++;
		if (it == m_SwitchableDirections.end())
			it = m_SwitchableDirections.begin();

		Vector2 direction = *it;
		if (Math::Dot(clickedTileDirection, direction) > 0.5f)
		{
			continue;
		}

		Vector2 nextTile = clickedTile + direction;
		int nextTileIndex = GetTileIndex(nextTile);

		if (!IsTileCoordinateValid(mapData, nextTile))
		{
			continue;
		}

		Vector2 nextTileDirection = mapData.at(nextTileIndex).direction;
		if (Math::Dot(nextTileDirection, direction) > 0.5f)
		{
			mapData[clickedTileIndex].direction = direction;
			break;
		}
	}

	mapData[clickedTileIndex].isDirty = true;
}

}

