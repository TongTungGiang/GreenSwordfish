#pragma once

#include "SystemBase.h"

namespace GS
{
class CarDestroySystem : public SystemBase
{
public:
	CarDestroySystem(GameWorld* world) : SystemBase(world) {}
	virtual void Update(Registry& registry, float deltaTime) override;
};
}

