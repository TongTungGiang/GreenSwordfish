#pragma once
#include <SystemBase.h>

namespace GS
{
class UISortSystem : public SystemBase
{
public:
	UISortSystem(GameWorld* world) : SystemBase(world) { }
	virtual void Update(Registry& registry, float deltaTime) override;
	virtual void Init(Registry& registry) override;
};
}
