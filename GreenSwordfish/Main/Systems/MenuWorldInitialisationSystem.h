#pragma once

#include "SystemBase.h"

namespace GS
{
class MenuWorldInitialisationSystem : public SystemBase
{

public:
	MenuWorldInitialisationSystem(class GameWorld* world) : SystemBase(world) { }

	virtual void Init(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override { }
	virtual void Exit(Registry& registry) override;

private:
	std::string m_CurrentlySelectedMap;
};
}
