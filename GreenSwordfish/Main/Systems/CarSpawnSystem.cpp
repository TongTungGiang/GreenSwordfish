#include "CarSpawnSystem.h"

#include "Components/CarTypeComponent.h"
#include "Components/TransformComponent.h"
#include "Components/SpriteComponent.h"
#include "Components/MoveForwardComponent.h"

#include "Data/CarData.h"
#include "Data/MapData.h"

#include <random>
#include "Components/CarDestinationComponent.h"
#include "Components/CarInformationRelationshipComponent.h"
#include <Components/UISortLayerComponent.h>

namespace GS
{

void CarSpawnSystem::Init(Registry& registry)
{
	const MapTileArray& map = registry.ctx<MapTileArray>();
	for (auto it = map.begin(); it != map.end(); it++)
	{
		if (it->second.type == TileType::StartPoint)
		{
			m_StartIndices.push_back(it->first);
		}

		if (it->second.type >= TileType::Building1 && it->second.type <= TileType::Building4)
		{
			m_DestinationTypes.insert(it->second.type);
		}
	}

	std::stringstream debug;
	debug << "CarSpawnSystem::Init(): " << m_StartIndices.size() << " start points, " << m_DestinationTypes.size() << " destination types";
	Debug::Log(debug.str());

	m_RemainingTime = CAR_SPAWN_INTERVAL;
}


void CarSpawnSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();

	if (m_RemainingTime > 0)
	{
		m_RemainingTime -= deltaTime;
		return;
	}

	m_RemainingTime = CAR_SPAWN_INTERVAL;

	// Init random stuff
	std::random_device rd;
	std::mt19937 gen(rd());

	// Spawn the car entity
	std::uniform_int_distribution<int> randomIndexDistribution(0, m_StartIndices.size() - 1);
	int randomStartIndex = randomIndexDistribution(gen);

	Vector2 startCoordinate = GetTileCoordinate(m_StartIndices[randomStartIndex]);
	Vector2 startPos = Math::TileCoordinateToWorld((int)startCoordinate.x, (int)startCoordinate.y);
	auto carEntity = registry.create();

	TransformComponent transform = TransformComponent(Vector2::east, startPos, 5, Layers::PROPS);
	registry.emplace<TransformComponent>(carEntity, transform);

	std::uniform_int_distribution<int> carTypeDistribution(1, CAR_TYPE_COUNT);
	int carType = carTypeDistribution(gen);
	registry.emplace<CarTypeComponent>(carEntity, CarTypeComponent(carType));
	registry.emplace<SpriteComponent>(carEntity, SpriteComponent(Rect(50, 60, 32, 26)));
	registry.emplace<MoveForwardComponent>(carEntity, MoveForwardComponent(startPos, CAR_MOVE_SPEED));

	std::uniform_int_distribution<int> carDestinationDistribution((int)TileType::Building1, (int)TileType::Building4);
	TileType destination = TileType::Building1;
	do
	{
		destination = (TileType)carDestinationDistribution(gen);
	} while (std::find(m_DestinationTypes.begin(), m_DestinationTypes.end(), destination) == m_DestinationTypes.end());
	 
	registry.emplace<CarDestinationComponent>(carEntity, CarDestinationComponent(destination));

	// Spawn the information entity
	auto informationEntity = registry.create();
	SpriteComponent targetSprite(Rect(0,0, 24, 24));
	targetSprite.SetTexture(g_TileSprites[destination]);
	registry.emplace<SpriteComponent>(informationEntity, targetSprite);
	registry.emplace<UISortLayerComponent>(informationEntity, UISortLayerComponent(UISortLayer::Destination));
	auto calloutBackgroundEntity = registry.create();

	SpriteComponent calloutSprite(Rect(0, 0, 32, 26));
	calloutSprite.SetTexture("callout.png");
	registry.emplace<SpriteComponent>(calloutBackgroundEntity, calloutSprite);
	registry.emplace<UISortLayerComponent>(calloutBackgroundEntity, UISortLayerComponent(UISortLayer::Callout));

	// Spawn the relationship entity
	auto relationshipEntity = registry.create();
	registry.emplace<CarInformationRelationshipComponent>(relationshipEntity, 
		CarInformationRelationshipComponent(carEntity, informationEntity, calloutBackgroundEntity));
}

void CarSpawnSystem::Exit(Registry& registry)
{
	m_StartIndices.clear();
}

}

