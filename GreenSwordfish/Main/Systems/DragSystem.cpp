#include "DragSystem.h"

#include "Components/InputDataComponent.h"

#include "Data/DragData.h"

#include "Events/ClickEvent.h"

#include "Systems/InputSystem.h"

namespace GS
{

void DragSystem::Init(Registry& registry)
{
	m_World->BindEvent<ClickEvent, DragSystem, &DragSystem::OnClick>(this);
	m_World->BindEvent<ClickReleaseEvent, DragSystem, &DragSystem::OnClickRelease>(this);
}

void DragSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();
	DragData* ptr = registry.try_ctx<DragData>();
	if (ptr != nullptr)
	{
		Entity inputEntity = InputSystem::GetInputEntity(m_World->GetRegistry());
		const InputDataComponent& input = m_World->GetRegistry().get<InputDataComponent>(inputEntity);
		ptr->currentPosition = input.mouse;
	}
}

void DragSystem::OnClick(const ClickEvent& ev)
{
	Entity inputEntity = InputSystem::GetInputEntity(m_World->GetRegistry());
	const InputDataComponent& input = m_World->GetRegistry().get<InputDataComponent>(inputEntity);
	DragData drag;
	drag.currentPosition = input.mouse;
	drag.startPosition = input.mouse;
	m_World->GetRegistry().set<DragData>(drag);
}

void DragSystem::OnClickRelease(const ClickReleaseEvent& ev)
{
	m_World->GetRegistry().unset<DragData>();
}

}

