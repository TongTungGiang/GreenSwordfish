#pragma once

#include "SystemBase.h"

namespace GS
{
/**
 * Assumption: this is called AFTER MapEditorInitialisationSystem
 */
class UpdateMapSpriteSystem : public SystemBase
{

public:
	UpdateMapSpriteSystem(class GameWorld* world) : SystemBase(world) { }

	virtual void Update(Registry& registry, float deltaTime) override;

};
}
