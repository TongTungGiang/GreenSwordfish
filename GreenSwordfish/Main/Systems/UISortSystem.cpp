#include "UISortSystem.h"

#include "Components/UISortLayerComponent.h"

namespace GS
{

typedef bool(*Comparer)(const UISortLayerComponent& lhs, const UISortLayerComponent& rhs);

static bool ShouldLhsStayInFrontOfRhs(const UISortLayerComponent& lhs, const UISortLayerComponent& rhs)
{
	return lhs.value > rhs.value;
}

static bool ShouldLhsStayBehindRhs(const UISortLayerComponent& lhs, const UISortLayerComponent& rhs)
{
	return !ShouldLhsStayInFrontOfRhs(lhs, rhs);
}

void UISortSystem::Init(Registry& registry)
{
	registry.sort<UISortLayerComponent, Comparer, entt::std_sort>(&ShouldLhsStayBehindRhs);
}

void UISortSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();

	{
		PROFILE_SCOPE("UISortSystem::Update::LayerInsertionSort");
		registry.sort<UISortLayerComponent, Comparer, entt::insertion_sort>(&ShouldLhsStayBehindRhs);
	}

	{
		PROFILE_SCOPE("UISortSystem::Update::SpriteSortBasedOnLayer");
		registry.sort<UISortLayerComponent, UISortLayerComponent>();
	}
}

}
