#pragma once

#include "SystemBase.h"

namespace GS
{
class MoveForwardSystem : public SystemBase
{

public:
	MoveForwardSystem(GameWorld* world) : SystemBase(world) {}
	virtual void Update(Registry& registry, float deltaTime) override;

};
}
