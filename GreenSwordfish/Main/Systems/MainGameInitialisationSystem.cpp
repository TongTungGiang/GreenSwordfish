#include "MainGameInitialisationSystem.h"

#include "Utilities/MapHelpers.h"
#include "Utilities/UIHelpers.h"

#include "Data/MapData.h"

#include "Components/SpriteComponent.h"
#include "Components/TileComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/TextComponent.h"
#include "Components/TransformComponent.h"
#include "Components/ClickableComponent.h"
#include "Components/UISortLayerComponent.h"

#include "WorldManager.h"
#include "Game.h"

using namespace sdlgui;

namespace GS
{

static void SpawnMapTile(Registry& registry, int x, int y, TileType type)
{
	Vector2 world = Math::TileCoordinateToWorld(x, y);
	Vector2 iso = Math::WorldToIso(world);
	Vector2 tileSize = (type == TileType::Blank || type == TileType::BlankGrass) ?
		Vector2(TILE_SPRITE_WIDTH_BLANK, TILE_SPRITE_HEIGHT_BLANK) :
		Vector2(TILE_SPRITE_WIDTH, TILE_SPRITE_HEIGHT);

	auto tileEntity = registry.create();
	registry.emplace<TileTypeComponent>(tileEntity, TileTypeComponent());
	SpriteComponent tileSprite(Rect(iso, tileSize));
	tileSprite.SetTexture(g_TileSprites[type]);
	registry.emplace<SpriteComponent>(tileEntity, tileSprite);
	registry.emplace<TileComponent>(tileEntity, TileComponent(x, y));

	if (IsBuilding(type))
	{
		registry.emplace<TransformComponent>(tileEntity, TransformComponent(Vector2::zero, world, 5, Layers::PROPS));
	}
	else
	{
		registry.emplace<TransformComponent>(tileEntity, TransformComponent(Vector2::zero, world, 0, Layers::GROUND));
		registry.emplace<BatchRenderingSpriteComponent>(tileEntity, BatchRenderingSpriteComponent());
	}
}

static void SpawnArrow(Registry& registry, int x, int y, TileType type, Vector2 direction)
{
	Vector2 world = Math::TileCoordinateToWorld(x, y);
	Vector2 iso = Math::WorldToIso(world);
	Vector2 arrowSize = Vector2(TILE_SPRITE_WIDTH_BLANK, TILE_SPRITE_HEIGHT_BLANK);

	auto tileArrowSpriteEntity = registry.create();
	registry.emplace<TileComponent>(tileArrowSpriteEntity, TileComponent(x, y));
	registry.emplace<ArrowComponent>(tileArrowSpriteEntity, ArrowComponent(direction));
	registry.emplace<TransformComponent>(tileArrowSpriteEntity, TransformComponent(Vector2::zero, world, 1, Layers::GROUND));
	SpriteComponent sprite = SpriteComponent(Rect(iso, arrowSize));
	sprite.SetTexture(GetArrowSpriteID(direction, type));
	registry.emplace<SpriteComponent>(tileArrowSpriteEntity, sprite);

	if (!IsJunction(type))
	{
		registry.emplace<BatchRenderingSpriteComponent>(tileArrowSpriteEntity, BatchRenderingSpriteComponent());
	}
}

void MainGameInitialisationSystem::Init(Registry& registry)
{
	InitUI(registry);
	InitMap(registry);
}

void MainGameInitialisationSystem::Exit(Registry& registry)
{
	Game::Get()->GetGUI()->reset();
}

void MainGameInitialisationSystem::InitUI(Registry& registry)
{
	auto gui = Game::Get()->GetGUI();

	constexpr int buttonSize = 150;
	constexpr int topRightMargin = 10;

	Window& window = gui->GetGuiWindow();
	window.button(
		"Back To Menu",
		[]()
		{
			Game::Get()->GetWorldManager()->LoadWorldSafe(WorldName::Menu);
		}
	)
		.withFixedWidth(buttonSize)
			.withPosition({ window.fixedWidth() - buttonSize - topRightMargin, topRightMargin });

		gui->performLayout();
}

void MainGameInitialisationSystem::InitMap(Registry& registry)
{
	MapHelper::LoadMap(registry, registry.ctx<MapLoadInfo>().loadedMap);

	const MapTileArray& mapData = registry.ctx<MapTileArray>();
	constexpr int maxIndexX = DEFAULT_MAP_SIZE - 1;
	constexpr int minIndexX = 0;
	constexpr int maxIndexY = DEFAULT_MAP_SIZE / 2 - 1;
	constexpr int minIndexY = -DEFAULT_MAP_SIZE / 2;
	for (int x = minIndexX; x <= maxIndexX; x++)
	{
		for (int y = minIndexY; y <= maxIndexY; y++)
		{
			int index = GetTileIndex(x, y);
			if (IsTileCoordinateValid(mapData, x, y))
			{
				auto type = mapData.at(index).type;
				auto direction = mapData.at(index).direction;

				SpawnMapTile(registry, x, y, type);

				if (!IsBuilding(type))
				{
					SpawnArrow(registry, x, y, type, direction);
				}
			}
			else
			{
				SpawnMapTile(registry, x, y, TileType::BlankGrass);
			}
		}
	}
}


}
