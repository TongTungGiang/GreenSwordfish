#pragma once

#include "SystemBase.h"
#include <Data/MapData.h>

namespace GS
{
class CarSpawnSystem : public SystemBase
{
public:
	CarSpawnSystem(GameWorld* world) : SystemBase(world), m_RemainingTime(0.0f) {}
	virtual void Init(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override;
	virtual void Exit(Registry& registry) override;

private:

	std::vector<int> m_StartIndices;
	std::set<TileType> m_DestinationTypes;
	float m_RemainingTime;

};
}
