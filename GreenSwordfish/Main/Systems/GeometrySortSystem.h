#pragma once

#include "SystemBase.h"

#include "Components/TransformComponent.h"

namespace GS
{
class GeometrySortSystem : public SystemBase
{
public:
	GeometrySortSystem(GameWorld* world) : SystemBase(world) { }
	virtual void Init(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override;
};
}
