#include "InputSystem.h"

#include "Components/Tags.h"
#include "Components/InputDataComponent.h"

#include "Events/ClickEvent.h"

namespace GS
{

static InputState UpdateInputState(bool pressedRawState, InputState prevState)
{
	if (true == pressedRawState)
	{
		if (prevState == InputState::JustDown)
			return InputState::Down;
		if (prevState == InputState::Up || prevState == InputState::JustUp) 
			return InputState::JustDown;
		else return InputState::Down;
	}
	else
	{
		if (prevState == InputState::JustUp)
			return InputState::Up;
		if (prevState == InputState::Down || prevState == InputState::JustDown)
			return InputState::JustUp;
		else return InputState::Up;
	}
}

Entity InputSystem::GetInputEntity(Registry& registry)
{
	auto view = registry.view<InputSingletonLabel>();
	return *view.begin();
}

void InputSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();

	auto& input = registry.get<InputDataComponent>(GetInputEntity(registry));
	input.leftButtonState = UpdateInputState(input.leftButton, input.leftButtonState);
	input.rightButtonState = UpdateInputState(input.rightButton, input.rightButtonState);

	if (input.leftButtonState == InputState::JustDown)
	{
		m_World->InvokeEvent<ClickEvent>(true, ClickEvent(input.mouse));
	}

	if (input.leftButtonState == InputState::JustUp)
	{
		m_World->InvokeEvent<ClickReleaseEvent>(true, ClickReleaseEvent());
	}
}

void InputSystem::Init(Registry& registry)
{
	auto entity = registry.create();
	registry.emplace<InputSingletonLabel>(entity);
	registry.emplace<InputDataComponent>(entity, InputDataComponent());
	registry.emplace<TextInputDataComponent>(entity, TextInputDataComponent());
}

void InputSystem::HandleSDLEvent(Registry& registry, SDL_Event& event)
{
	switch (event.type)
	{
	case SDL_MOUSEBUTTONDOWN:
		InputSystem::RegisterMouseButtonDown(registry, event.button.button);
		break;
	case SDL_MOUSEBUTTONUP:
		InputSystem::RegisterMouseButtonUp(registry, event.button.button);
		break;
	case SDL_MOUSEMOTION:
		InputSystem::RegisterMouseMotion(registry, event.motion.x, event.motion.y);
		break;
	case SDL_KEYDOWN:
		if (event.key.keysym.sym == SDLK_BACKSPACE)
		{
			InputSystem::HandleBackspace(registry);
		}
		else if (event.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL)
		{
			InputSystem::HandleCopy(registry);
		}
		else if (event.key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL)
		{
			InputSystem::HandlePaste(registry);
		}
		break;
	case SDL_TEXTINPUT:
		InputSystem::HandleNewCharacter(registry, event.text.text);
		break;
	}
}

void InputSystem::RegisterMouseButtonDown(Registry& registry, int button)
{
	auto& inputs = registry.get<InputDataComponent>(GetInputEntity(registry));
	if (button == SDL_BUTTON_LEFT) inputs.leftButton = true;
	else if (button == SDL_BUTTON_RIGHT) inputs.rightButton = true;
}

void InputSystem::RegisterMouseButtonUp(Registry& registry, int button)
{
	auto& inputs = registry.get<InputDataComponent>(GetInputEntity(registry));
	if (button == SDL_BUTTON_LEFT) inputs.leftButton = false;
	else if (button == SDL_BUTTON_RIGHT) inputs.rightButton = false;
}

void InputSystem::RegisterMouseMotion(Registry& registry, int mouseX, int mouseY)
{
	auto& inputs = registry.get<InputDataComponent>(GetInputEntity(registry));
	inputs.mouse.x = (float)mouseX;
	inputs.mouse.y = (float)mouseY;
}

void InputSystem::HandleNewCharacter(Registry& registry, char* c)
{
	auto& textInput = registry.get<TextInputDataComponent>(GetInputEntity(registry));
	textInput.value += c;
}

void InputSystem::HandleBackspace(Registry& registry)
{
	auto& textInput = registry.get<TextInputDataComponent>(GetInputEntity(registry));
	if (textInput.value.length() > 0)
	{
		textInput.value.pop_back();
	}
}

void InputSystem::HandleCopy(Registry& registry)
{
	auto& textInput = registry.get<TextInputDataComponent>(GetInputEntity(registry));
	SDL_SetClipboardText(textInput.value.c_str());
}

void InputSystem::HandlePaste(Registry& registry)
{
	// Get raw text in clipboard
	std::string clipboardText = SDL_GetClipboardText();

	// Trim the first line
	std::size_t firstNewline = clipboardText.find_first_of('\n');
	if (firstNewline != std::string::npos)
	{
		clipboardText = clipboardText.substr(0, firstNewline - 1);
	}

	auto& textInput = registry.get<TextInputDataComponent>(GetInputEntity(registry));
	textInput.value = clipboardText;
}

}
