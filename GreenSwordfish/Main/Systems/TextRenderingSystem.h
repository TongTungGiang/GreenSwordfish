#pragma once

#include "RenderingSystemBase.h"

namespace GS
{
class TextRenderingSystem : public RenderingSystemBase
{
public:
	TextRenderingSystem(Renderer* renderer) : RenderingSystemBase(renderer) { }
	virtual void Update(Registry& registry) override;
};
}
