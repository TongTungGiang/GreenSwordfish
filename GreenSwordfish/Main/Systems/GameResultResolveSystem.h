#pragma once
#include "SystemBase.h"

namespace GS
{
class GameResultResolveSystem : public SystemBase
{
public:
	GameResultResolveSystem(GameWorld* world) : SystemBase(world) {}
	virtual void Init(Registry& registry) override;
	virtual void Exit(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override {}
private:
	void OnTimesUp(const struct TimesUpEvent&);
};
}
