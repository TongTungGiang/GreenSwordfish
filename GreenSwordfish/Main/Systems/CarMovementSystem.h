#pragma once

#include "SystemBase.h"

namespace GS
{
class CarMovementSystem : public SystemBase
{

public:
	CarMovementSystem(GameWorld* world) : SystemBase(world) {}
	virtual void Update(Registry& registry, float deltaTime) override;

};
}
