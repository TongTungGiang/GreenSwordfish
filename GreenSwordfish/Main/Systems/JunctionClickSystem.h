#pragma once

#include "SystemBase.h"

namespace GS
{
class JunctionClickSystem : public SystemBase
{
public:
	JunctionClickSystem(GameWorld* world);
	virtual void Update(Registry& registry, float deltaTime) override { }
	virtual void Init(Registry& registry) override;
	virtual void Exit(Registry& registry) override;

private:
	void ProcessClick(const struct ClickEvent& ev);

	std::vector<Vector2> m_SwitchableDirections;
};
}
