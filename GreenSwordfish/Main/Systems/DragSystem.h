#pragma once

#include "SystemBase.h"

namespace GS
{
class DragSystem : public SystemBase 
{
public:
	DragSystem(class GameWorld* world) : SystemBase(world) {}
	virtual void Init(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override;

private:
	void OnClick(const struct ClickEvent& ev);
	void OnClickRelease(const struct ClickReleaseEvent& ev);
};
}
