#pragma once

#include "SystemBase.h"

namespace GS
{
class TileCursorFollowMouseSystem : public SystemBase
{
public:
	TileCursorFollowMouseSystem(GameWorld* world) : SystemBase(world) { }

	virtual void Init(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override;
};
}

