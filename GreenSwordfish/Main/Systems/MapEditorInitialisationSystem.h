#pragma once

#include "SystemBase.h"

namespace GS
{
class MapEditorInitialisationSystem : public SystemBase
{
public:
	MapEditorInitialisationSystem(GameWorld* world) : SystemBase(world) { }
	virtual void Init(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override { }
	virtual void Exit(Registry& registry) override;

private:
	void LoadBlankMap(Registry& registry);
	void InitUI(Registry& registry);
};
};
