#include "UpdateTextInputSystem.h"

#include "Components/TextComponent.h"
#include "Components/InputDataComponent.h"

#include "Systems/InputSystem.h"
#include "Data/InputData.h"

namespace GS
{

void UpdateTextInputSystem::Update(Registry& registry, float deltaTime)
{
	TextInputDataComponent textInput = registry.get<TextInputDataComponent>(InputSystem::GetInputEntity(registry));
	const CurrentlyEditingText& currentlyEditingTextData = registry.ctx<CurrentlyEditingText>();
	TextComponent& text = registry.get<TextComponent>(currentlyEditingTextData.entity);
	text.value = textInput.value;
}

}

