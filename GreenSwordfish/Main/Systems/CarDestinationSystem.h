#pragma once

#include "SystemBase.h"

namespace GS
{
class CarDestinationSystem : public SystemBase
{
public:
	CarDestinationSystem(GameWorld* world) : SystemBase(world) {}
	virtual void Update(Registry& registry, float deltaTime) override;

};
}
