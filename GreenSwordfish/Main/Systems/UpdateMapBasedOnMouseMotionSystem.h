#pragma once

#include "SystemBase.h"

namespace GS
{

struct ClickEvent;

class UpdateMapBasedOnMouseMotionSystem : public SystemBase
{
public:
	UpdateMapBasedOnMouseMotionSystem(GameWorld* world) : SystemBase(world) { }

	virtual void Update(Registry& registry, float deltaTime) override;

private:
	bool DrawTileLine(Registry& registry);
	void SetupSpecialTiles(Registry& registry);
};

}
