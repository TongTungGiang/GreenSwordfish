#pragma once

#include "SystemBase.h"

namespace GS
{
class UpdateTextInputSystem : public SystemBase
{
public:
	UpdateTextInputSystem(GameWorld* world) : SystemBase(world) {}
	virtual void Update(Registry& registry, float deltaTime) override;
};
}
