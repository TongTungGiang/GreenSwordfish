#pragma once

#include "RenderingSystemBase.h"

namespace GS
{

class SpriteRenderingSystem : public RenderingSystemBase
{

public:
	SpriteRenderingSystem(Renderer* renderer) : RenderingSystemBase(renderer) { }
	virtual void Update(Registry& registry) override;

};

}

