#include "ScoreDisplaySystem.h"
#include "Components/TextComponent.h"
#include "Data/GameScoreData.h"
#include "Data/MapData.h"
#include "Utilities/UIHelpers.h"

#include "Game.h"

namespace GS
{

void ScoreDisplaySystem::Init(Registry& registry)
{
	registry.set<ScoreData>(ScoreData());

	MapGameplayInfo gameplayInfo = registry.ctx<MapGameplayInfo>();
	m_TargetScore = gameplayInfo.targetScore;

	sdlgui::Window& window = Game::Get()->GetGUI()->GetGuiWindow();
	m_ScoreLabel = &window.label("Score: 0/" + std::to_string(m_TargetScore));
	m_ScoreLabel->withPosition({10, 10});
}

void ScoreDisplaySystem::Update(Registry& registry, float deltaTime)
{
	const ScoreData& scoreData = registry.ctx<ScoreData>();
	m_ScoreLabel->setCaption("Score: " + std::to_string(scoreData.currentScore) 
		+ "/" + std::to_string(m_TargetScore));
}

}

