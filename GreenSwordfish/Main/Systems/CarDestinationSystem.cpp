#include "CarDestinationSystem.h"

#include "Components/CarInformationRelationshipComponent.h"
#include "Components/SpriteComponent.h"

namespace GS
{

void CarDestinationSystem::Update(Registry& registry, float deltaTime)
{
	std::vector<Entity> entitiesToDestroy;

	registry.view<CarInformationRelationshipComponent>().each(
		[&registry, &entitiesToDestroy](auto relationshipEntity, const CarInformationRelationshipComponent& relationship)
		{
			if (!registry.valid(relationship.car))
			{
				entitiesToDestroy.push_back(relationshipEntity);
				entitiesToDestroy.push_back(relationship.information);
				entitiesToDestroy.push_back(relationship.calloutBackground);
				return;
			}

			const SpriteComponent& carSprite = registry.get<SpriteComponent>(relationship.car);
			SpriteComponent& calloutSprite = registry.get<SpriteComponent>(relationship.calloutBackground);
			SpriteComponent& destinationSprite = registry.get<SpriteComponent>(relationship.information);

			Vector2 calloutSpritePosition = Vector2(30, -20) + carSprite.renderRect.GetCoord();
			calloutSprite.renderRect.SetCoord(calloutSpritePosition);

			Vector2 destinationSpritePosition = Vector2(35, -25) + carSprite.renderRect.GetCoord();
			destinationSprite.renderRect.SetCoord(destinationSpritePosition);
		}
	);

	for (Entity e : entitiesToDestroy)
	{
		registry.destroy(e);
	}
}

}

