#include "CarMovementSystem.h"

#include "Data/CarData.h"
#include "Data/MapData.h"

#include "Components/TransformComponent.h"
#include "Components/SpriteComponent.h"
#include "Components/CarTypeComponent.h"
#include "Components/MoveForwardComponent.h"


namespace GS
{
void CarMovementSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();

	const MapTileArray& map = registry.ctx<MapTileArray>();
	registry.view<CarTypeComponent, TransformComponent, SpriteComponent, MoveForwardComponent>().each(
		[&map](auto entity, const CarTypeComponent& car, TransformComponent& transform, SpriteComponent& sprite, MoveForwardComponent& moveForward)
		{
			// Setting up direction and target
			if (transform.position == moveForward.target)
			{
				Vector2 currentTileCoordinate = Math::WorldToTileCoordinate(transform.position);
				int currentTileIndex = GetTileIndex(currentTileCoordinate);

				if (map.find(currentTileIndex) != map.end())
				{
					Vector2 currentTileDirection = map.at(currentTileIndex).direction;

					transform.direction = currentTileDirection;

					// Calculate the next cell position (raw)
					Vector2 nextTileCoordinate = currentTileCoordinate + currentTileDirection;
					Vector2 nextTilePosition = Math::TileCoordinateToWorld((int)nextTileCoordinate.x, (int)nextTileCoordinate.y);

					moveForward.target = nextTilePosition;
				}
			}

			// Update car sprite based on the current direction
			Vector2 iso = Math::WorldToIso(transform.position + Vector2(TILE_WORLD_SIZE /4, -TILE_WORLD_SIZE / 4));
			Vector2 size = sprite.renderRect.GetSize();
			sprite.renderRect = Rect(iso, size);
			sprite.SetTexture(GetCarSprite(car.value, transform.direction));
		}
	);
}
}
