#pragma once
#include "SystemBase.h"

namespace GS
{
class TimerSystem : public SystemBase
{
public:
	TimerSystem(GameWorld* world) : SystemBase(world), m_PlayTime(0) { }

	virtual void Init(Registry& registry) override;
	virtual void Update(Registry& registry, float deltaTime) override;

private:
	int m_PlayTime;
	sdlgui::Label* m_TimerLabel;
};
}
