#include "CarDestroySystem.h"

#include "Data/MapData.h"

#include "Components/CarTypeComponent.h"
#include "Components/TransformComponent.h"
#include "Components/CarDestinationComponent.h"

#include "Data/GameScoreData.h"

namespace GS
{

void CarDestroySystem::Update(Registry& registry, float deltaTime)
{
	const MapTileArray& map = registry.ctx<MapTileArray>();
	registry.view<CarTypeComponent, CarDestinationComponent, TransformComponent>().each(
		[&map, &registry](auto entity, const CarTypeComponent& car, const CarDestinationComponent& destination, TransformComponent& transform)
		{
			Vector2 currentTileCoordinate = Math::WorldToTileCoordinate(transform.position);
			int currentTileIndex = GetTileIndex(currentTileCoordinate);

			try
			{
				TileType currentTileType = map.at(currentTileIndex).type;
				if (currentTileType >= TileType::Building1 && currentTileType <= TileType::Building4)
				{
					if (destination.value == currentTileType)
					{
						ScoreData& scoreData = registry.ctx<ScoreData>();
						scoreData.currentScore++;

						Debug::Log("Right destination", Debug::DebugTextColor::Green);
					}
					else
					{
						ScoreData& scoreData = registry.ctx<ScoreData>();
						scoreData.currentScore--;
						if (scoreData.currentScore < 0)
						{
							scoreData.currentScore = 0;
						}

						Debug::Log("Wrong destination", Debug::DebugTextColor::Cyan);
					}
					registry.destroy(entity);
				}
			}
			catch (std::out_of_range& e)
			{
				std::stringstream debug;
				debug << "It's weird that index " << currentTileCoordinate << " doesn't exist in the map";
				Debug::LogError(debug.str());
			}
		}
	);
}

}

