#include "MenuWorldInitialisationSystem.h"

#include "Components/SpriteComponent.h"
#include "Components/ClickableComponent.h"
#include "Components/TextComponent.h"

#include "Game.h"
#include "WorldManager.h"

#include "Utilities/UIHelpers.h"
#include "Utilities/MapHelpers.h"

#include "sdlgui/layout.h"
#include "Data/MapData.h"

using namespace sdlgui;

namespace GS
{

void MenuWorldInitialisationSystem::Init(Registry& registry)
{
	auto backgroundSprite = registry.create();
	SpriteComponent sprite = SpriteComponent(Rect(0, 0, WIDTH, HEIGHT));
	sprite.SetTexture("background.png");
	registry.emplace<SpriteComponent>(backgroundSprite, sprite);


	auto gui = Game::Get()->GetGUI();

	Window& window = gui->GetGuiWindow();
	int margin = window.fixedSize().x / 4;
	int buttonWidth = window.fixedSize().x / 2;
	window.setLayout(new BoxLayout(Orientation::Vertical, Alignment::Fill, margin, 50));

	window.button(
		"Map Editor",
		[]()
		{
			Game::Get()->GetWorldManager()->LoadWorldSafe(WorldName::MapEditor);
		}
	).withFixedWidth(buttonWidth);
	window.button(
		"Game",
		[&]()
		{
			auto& mapSelectionWindow = window.wdg<Window>("Select Map");
			mapSelectionWindow.setTheme(new Theme(Game::Get()->GetGlobalSDLRenderer()));
			mapSelectionWindow.setLayout(new BoxLayout(Orientation::Vertical, Alignment::Fill, 10, 10));

			{
				auto& mapListScrollView = mapSelectionWindow.vscrollpanel();
				mapListScrollView.setFixedHeight(250);

				auto& mapListPanel = mapListScrollView.widget();
				mapListPanel.setLayout(new BoxLayout(Orientation::Vertical, Alignment::Fill, 10, 10));

				auto maps = MapHelper::ListAllMapsInMapDirectory();
				m_CurrentlySelectedMap = *maps.begin();
				for (auto mapIt = maps.begin(); mapIt != maps.end(); mapIt++)
				{
					std::string currentMap = (*mapIt);
					auto& button = mapListPanel.button(currentMap)
						.withFlags(Button::RadioButton)
						.withCallback([this, currentMap]()
							{
								this->m_CurrentlySelectedMap = currentMap;
							})
						.withFixedWidth(250)
								.withFontSize(14);
				}
			}

			{
				auto& commandPanel = mapSelectionWindow.widget()
					.withLayout<BoxLayout>(Orientation::Horizontal, Alignment::Fill, 10, 10);
				commandPanel.setSize(mapSelectionWindow.size());
				commandPanel.button("GO!").withCallback([this]()
					{
						Game::Get()->GetWorldManager()->LoadWorldSafe(WorldName::MainGame,
							[this](Registry& registry)
							{
								registry.set<MapLoadInfo>(this->m_CurrentlySelectedMap);
							}
						);
					}
				).withFixedSize({ 120, 40 });

				commandPanel.button("Cancel").withCallback([&]()
					{
						mapSelectionWindow.dispose();
					}
				).withFixedSize({ 120, 40 });
				//commandPanel.performLayout(Game::Get()->GetGlobalSDLRenderer());
			}

			//mapSelectionWindow.performLayout(Game::Get()->GetGlobalSDLRenderer());
			mapSelectionWindow.center();
			mapSelectionWindow.setFocused(true);
		}
	).withFixedWidth(buttonWidth);
	window.button(
		"Exit",
		[]()
		{
			SDL_Event quitEv;
			quitEv.type = SDL_QUIT;
			SDL_PushEvent(&quitEv);
		}
	).withFixedWidth(buttonWidth);

	gui->performLayout();
}

void MenuWorldInitialisationSystem::Exit(Registry& registry)
{
	Game::Get()->GetGUI()->reset();
}

}
