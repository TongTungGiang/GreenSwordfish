#pragma once
#include "SystemBase.h"

namespace GS
{

class ScoreDisplaySystem : public SystemBase
{
public:
	ScoreDisplaySystem(GameWorld* world) : SystemBase(world), m_TargetScore(0) {}
	virtual void Update(Registry& registry, float deltaTime) override;
	virtual void Init(Registry& registry) override;

private:
	int m_TargetScore;
	sdlgui::Label* m_ScoreLabel;
};
}
