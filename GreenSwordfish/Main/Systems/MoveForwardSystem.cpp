#include "MoveForwardSystem.h"


#include "Components/TransformComponent.h"
#include "Components/MoveForwardComponent.h"

namespace GS
{

void MoveForwardSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();
	registry.view<TransformComponent, MoveForwardComponent>().each(
		[deltaTime] (auto entity, TransformComponent& transform, MoveForwardComponent& moveForward)
		{
			Vector2 delta = moveForward.target - transform.position;
			float maxMovement = deltaTime * moveForward.speed;
			if (Math::Magnitude(delta) <= maxMovement)
			{
				transform.position = moveForward.target;
			}
			else
			{
				Vector2 movement = Math::Normalize(delta) * moveForward.speed * deltaTime;
				transform.position += movement;
			}
		}
	);
}

}

