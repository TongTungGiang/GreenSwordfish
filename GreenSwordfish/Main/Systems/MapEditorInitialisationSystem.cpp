#include "MapEditorInitialisationSystem.h"

#include "Components/SpriteComponent.h"
#include "Components/TileComponent.h"
#include "Components/ArrowComponent.h"

#include "Components/TextComponent.h"
#include "Components/IsometricClickableComponent.h"
#include "Components/ClickableComponent.h"

#include "Data/MapData.h"
#include "Data/InputData.h"

#include "Utilities/MapHelpers.h"
#include "Utilities/UIHelpers.h"
#include "WorldManager.h"
#include "Game.h"

using namespace sdlgui;

namespace GS
{

void MapEditorInitialisationSystem::Init(Registry& registry)
{
	InitUI(registry);
	LoadBlankMap(registry);
}

void MapEditorInitialisationSystem::Exit(Registry& registry)
{
	Game::Get()->GetGUI()->reset();
}

void MapEditorInitialisationSystem::LoadBlankMap(Registry& registry)
{
	MapTileArray mapData;
	registry.set<MapTileArray>(mapData);

	constexpr int maxIndexX = DEFAULT_MAP_SIZE - 1;
	constexpr int minIndexX = 0;
	constexpr int maxIndexY = DEFAULT_MAP_SIZE / 2 - 1;
	constexpr int minIndexY = -DEFAULT_MAP_SIZE / 2;
	for (int x = maxIndexX; x >= minIndexX; x--)
	{
		for (int y = maxIndexY; y >= minIndexY; y--)
		{
			MapHelper::SpawnMapTile(registry, x, y, TileType::Blank, Vector2::zero);
		}
	}
}

void MapEditorInitialisationSystem::InitUI(Registry& registry)
{
	auto gui = Game::Get()->GetGUI();

	constexpr int buttonSize = 150;
	constexpr int topRightMargin = 10;

	Window& window = gui->GetGuiWindow();
	window.button(
		"Back To Menu",
		[]()
		{
			Game::Get()->GetWorldManager()->LoadWorldSafe(WorldName::Menu);
		})
		.withFixedWidth(buttonSize)
		.withPosition({ window.fixedWidth() - buttonSize - topRightMargin, topRightMargin });
	
	auto& mapInfoPanel = window.wdg<Window>("");
	Theme* panelTheme = new Theme(Game::Get()->GetGlobalSDLRenderer());
	panelTheme->mWindowDropShadowSize = 0;
	panelTheme->mWindowHeaderHeight = 0;
	panelTheme->mBorderDark = { 0, 0,0,0 };
	mapInfoPanel.setTheme(panelTheme);
	auto* layout = new GridLayout(Orientation::Horizontal, 2, Alignment::Middle, 10, 5);
	layout->setColAlignment({ Alignment::Maximum, Alignment::Fill });
	mapInfoPanel.setLayout(layout);
	   	
	mapInfoPanel.add<Label>("Map name:");
	auto& mapNameTextBox = mapInfoPanel.wdg<TextBox>();
	mapNameTextBox.setEditable(true);
	mapNameTextBox.setFixedSize(sdlgui::Vector2i(100, 20));
	mapNameTextBox.setValue("testmap");
	mapNameTextBox.setUnits("");
	mapNameTextBox.setDefaultValue("0.0");
	mapNameTextBox.setFontSize(16);
	mapNameTextBox.setAlignment(TextBox::Alignment::Left);

	mapInfoPanel.add<Label>("Play time:");
	auto& playTimeTextBox = mapInfoPanel.wdg<TextBox>();
	playTimeTextBox.setEditable(true);
	playTimeTextBox.setFixedSize(sdlgui::Vector2i(100, 20));
	playTimeTextBox.setValue("60");
	playTimeTextBox.setUnits("s");
	playTimeTextBox.setDefaultValue("0.0");
	playTimeTextBox.setFontSize(16);
	playTimeTextBox.setFormat("[1-9][0-9]*");
	playTimeTextBox.setAlignment(TextBox::Alignment::Left);

	mapInfoPanel.add<Label>("Target score:");
	auto& targetScoreTextBox = mapInfoPanel.wdg<TextBox>();
	targetScoreTextBox.setEditable(true);
	targetScoreTextBox.setFixedSize(sdlgui::Vector2i(100, 20));
	targetScoreTextBox.setValue("5");
	targetScoreTextBox.setUnits("");
	targetScoreTextBox.setDefaultValue("0.0");
	targetScoreTextBox.setFontSize(16);
	targetScoreTextBox.setFormat("[1-9][0-9]*");
	targetScoreTextBox.setAlignment(TextBox::Alignment::Left);

	mapInfoPanel.button("Save map").withCallback(
		[&registry, &mapNameTextBox, &targetScoreTextBox, &playTimeTextBox]()
		{
			MapHelper::RandomiseMapEndPoints(registry);

			std::string mapName = mapNameTextBox.value();
			int playTime = std::stoi(playTimeTextBox.value());
			int targetScore = std::stoi(targetScoreTextBox.value());

			MapHelper::SaveMap(registry, mapName, targetScore, playTime);
		});

	gui->performLayout();
}

}

