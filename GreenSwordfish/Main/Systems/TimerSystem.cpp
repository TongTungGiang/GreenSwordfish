#include "TimerSystem.h"

#include "Data/TimerData.h"
#include "Data/MapData.h"
#include "Utilities/UIHelpers.h"
#include "Components/TextComponent.h"

#include "Events/TimesUpEvent.h"

#include "Game.h"

namespace GS
{

void TimerSystem::Init(Registry& registry)
{
	TimerData initialTimer;
	initialTimer.elapsedTime = 0;
	registry.set<TimerData>(initialTimer);

	MapGameplayInfo gameplayInfo = registry.ctx<MapGameplayInfo>();
	m_PlayTime = gameplayInfo.playTime;

	sdlgui::Window& window = Game::Get()->GetGUI()->GetGuiWindow();
	m_TimerLabel = &window.label("Elapsed: 0/" + std::to_string(m_PlayTime));
	m_TimerLabel->withPosition({ 10, 50 });
}

void TimerSystem::Update(Registry& registry, float deltaTime)
{
	TimerData& timerData = registry.ctx<TimerData>();
	timerData.elapsedTime += deltaTime;

	m_TimerLabel->setCaption("Elapsed: " + std::to_string((int)timerData.elapsedTime) 
		+ "/" + std::to_string(m_PlayTime));

	if (timerData.elapsedTime >= m_PlayTime)
	{
		m_World->InvokeEvent(false, TimesUpEvent());
	}
}


}
