#include "GeometrySortSystem.h"

#include "Components/TransformComponent.h"
#include <Components/SpriteComponent.h>

namespace GS
{

typedef bool(*Comparer)(const TransformComponent& lhs, const TransformComponent& rhs);

static bool ShouldLhsStayInFrontOfRhs(const TransformComponent& lhs, const TransformComponent& rhs)
{
	if (lhs.layer > rhs.layer) { return true; }
	else if (lhs.layer < rhs.layer) { return false; }

	if (lhs.position.x > rhs.position.x) { return true; }
	else if (lhs.position.x < rhs.position.x) { return false; }

	if (lhs.position.y > rhs.position.y) { return true; }
	else if (lhs.position.y < rhs.position.y) { return false; }

	return lhs.height > rhs.height;
}

static bool ShouldLhsStayBehindRhs(const TransformComponent& lhs, const TransformComponent& rhs)
{
	return !ShouldLhsStayInFrontOfRhs(lhs, rhs);
}

void GeometrySortSystem::Init(Registry& registry)
{
	registry.sort<TransformComponent, Comparer, entt::std_sort>(&ShouldLhsStayBehindRhs);
}

void GeometrySortSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();

	{
		PROFILE_SCOPE("GeometrySortSystem::Update::TransformInsertionSort");
		registry.sort<TransformComponent, Comparer, entt::insertion_sort>(&ShouldLhsStayBehindRhs);
	}

	{
		PROFILE_SCOPE("GeometrySortSystem::Update::SpriteSortBasedOnTransform");
		registry.sort<SpriteComponent, TransformComponent>();
	}
}

}

