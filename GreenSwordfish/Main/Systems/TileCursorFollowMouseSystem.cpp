#include "TileCursorFollowMouseSystem.h"

#include "InputSystem.h"

#include "Components/InputDataComponent.h"
#include "Components/Tags.h"
#include "Components/SpriteComponent.h"

#include "Data/MapData.h"

namespace GS
{

void TileCursorFollowMouseSystem::Init(Registry& registry)
{
	auto cursorEntity = registry.create();
	
	registry.emplace<CursorLabel>(cursorEntity, CursorLabel());	
	SpriteComponent cursorSprite(Rect(200, 200, TILE_SPRITE_WIDTH_BLANK, TILE_SPRITE_HEIGHT_BLANK));
	cursorSprite.SetTexture("cursor.png");
	registry.emplace<SpriteComponent>(cursorEntity, cursorSprite);
}

void TileCursorFollowMouseSystem::Update(Registry& registry, float deltaTime)
{
	PROFILE_SCOPE();
	const Entity inputEntity = InputSystem::GetInputEntity(registry);
	const InputDataComponent& input = registry.get<InputDataComponent>(inputEntity);

	Vector2 tile = Math::IsoToTileCoordinate(input.mouse);
	Vector2 tileWorldPos = Math::TileCoordinateToWorld((int)tile.x, (int)tile.y);
	Vector2 tilePos = Math::WorldToIso(tileWorldPos);

	auto cursorView = registry.view<CursorLabel>();
	Entity cursor = *cursorView.begin();

	SpriteComponent& cursorSprite = registry.get<SpriteComponent>(cursor);

	cursorSprite.renderRect.x = tilePos.x;
	cursorSprite.renderRect.y = tilePos.y;
}

}
