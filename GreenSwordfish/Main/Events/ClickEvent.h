#pragma once

#include "Math/Vector2.h"

namespace GS
{
struct ClickEvent
{
	Vector2 mousePosition;

	ClickEvent(Vector2 mousePosition) : mousePosition(mousePosition) { }
};

struct ClickReleaseEvent { };
}
