#pragma once

#include "ECS.h"
#include "ISystem.h"
#include "GameWorld.h"

namespace GS
{
class SystemBase : public ISystemBase
{
public:
	SystemBase(GameWorld* world) : m_World(world), m_IsEnabled(true) {}
	virtual ~SystemBase() { }
	virtual void Init(Registry& registry) { };
	virtual void Update(Registry& registry, float deltaTime) = 0;
	virtual void Exit(Registry& registry) { };

protected:
	GameWorld* m_World;
	bool m_IsEnabled;
};
}
