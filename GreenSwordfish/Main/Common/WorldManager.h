#pragma once

#include "GameWorld.h"
#include "Renderer.h"

namespace GS
{

//typedef void(*OnWorldLoadedFunc)(GameWorld* world);

typedef std::function<void(GameWorld*)> OnWorldLoadedFunc;

class WorldManager
{
public:
	WorldManager();
	~WorldManager();

	void Init();
	void Exit();
	void PostUpdate();

public:

	GameWorld* GetCurrentWorld() { return m_CurrentWorld; }

public:

	/**
	 * Cache the world loading command until the end of the loop.
	 */
	void LoadWorldSafe(const char* worldName, const AdditionalDataSetupFunc& dataSetupFunc = nullptr);

	/**
	 * Cache the world loading command until the end of the loop.
	 */
	void LoadWorldSafe(const std::string& worldName, const AdditionalDataSetupFunc& dataSetupFunc = nullptr);

	/**
	 * Load the intended world immediately, could break the game loop.
	 */
	void LoadWorldImmediate(const std::string& worldName, const AdditionalDataSetupFunc& dataSetupFunc = nullptr);

	/**
	 * Load the intended world immediately, could break the game loop.
	 */
	void LoadWorldImmediate(const char* worldName, const AdditionalDataSetupFunc& dataSetupFunc = nullptr);

	OnWorldLoadedFunc OnWorldLoaded;

private:
	std::vector<std::function<void()>> m_PostUpdateCommands;

private:

	typedef std::unordered_map<const char*, GameWorld*> WorldHashMap;
	WorldHashMap m_WorldHashMap;

	GameWorld* m_CurrentWorld;
};

class WorldName
{
public:
	static constexpr auto Menu = "Menu";
	static constexpr auto MapEditor = "MapEditor";
	static constexpr auto MainGame = "MainGame";
};
}
