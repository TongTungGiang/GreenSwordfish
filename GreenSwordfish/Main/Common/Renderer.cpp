#include "Renderer.h"

#include "ResourceManager.h"
#include "GameWorld.h"

#include "Systems/SpriteRenderingSystem.h"
#include "Systems/TextRenderingSystem.h"
#include "Systems/MapBatchRenderingSystem.h"
#include "Game.h"

namespace GS
{

Renderer::Renderer(ResourceManager* resourceManager, SDL_Renderer* renderer) :
	m_ResourceManager(resourceManager),
	m_SdlRenderer(renderer)
{
	SetupSystems();
}

Renderer::~Renderer()
{
	for (auto s : m_RenderingSystems)
	{
		delete s;
	}
}

void Renderer::Init()
{
	for (SystemArray::iterator it = m_RenderingSystems.begin(); it != m_RenderingSystems.end(); it++)
	{
		(*it)->Init(m_World->m_Registry);
	}
}

void Renderer::Render()
{
	PROFILE_SCOPE();


	SDL_SetRenderDrawColor(m_SdlRenderer, Color::white.r, Color::white.g, Color::white.b, Color::white.a);
	SDL_RenderClear(m_SdlRenderer);

	{
		PROFILE_SCOPE("SystemRendering");
		for (SystemArray::iterator it = m_RenderingSystems.begin(); it != m_RenderingSystems.end(); it++)
		{
			(*it)->Update(m_World->m_Registry);
		}
	}
	{
		PROFILE_SCOPE("GUIRendering");
		Game::Get()->GetGUI()->drawAll();
	}

	SDL_RenderPresent(m_SdlRenderer);
}

void Renderer::SetupSystems()
{
	m_RenderingSystems.push_back(new SpriteRenderingSystem(this));
}

void Renderer::SetupWorld(class GameWorld* world)
{
	m_World = world;
}

void Renderer::Exit()
{
	for (SystemArray::iterator it = m_RenderingSystems.begin(); it != m_RenderingSystems.end(); it++)
	{
		(*it)->Exit(m_World->m_Registry);
	}
}

}
