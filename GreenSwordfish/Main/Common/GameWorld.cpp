#include "GameWorld.h"

#include "SystemBase.h"

namespace GS
{

GameWorld::GameWorld() : m_DeltaTime(0)
{
}

GameWorld::~GameWorld()
{
	for (auto s : m_ActiveSystems)
	{
		delete s;
	}
}

void GameWorld::Init(const AdditionalDataSetupFunc& dataSetupFunc)
{
	m_Timepoint = std::chrono::steady_clock::now();
	std::srand(std::time(nullptr));

	m_DeltaTime = 0;

	m_Registry = Registry();
	if (dataSetupFunc != nullptr)
		dataSetupFunc(m_Registry);

	m_ActiveSystems.assign(m_OriginalSystems.begin(), m_OriginalSystems.end());
	m_DisabledSystems.clear();
	m_ToBeDisabledSystems.clear();
	m_ToBeEnabledSystems.clear();

	for (SystemArray::iterator it = m_ActiveSystems.begin(); it != m_ActiveSystems.end(); it++)
	{
		(*it)->Init(m_Registry);
	}
}

void GameWorld::Update()
{
	PROFILE_SCOPE();

	// Update time
	auto now = std::chrono::steady_clock::now();
	auto diff = now - m_Timepoint;
	m_Timepoint = now;
	m_DeltaTime = (float)((double)std::chrono::duration_cast<std::chrono::microseconds>(diff).count() / 1000000.0);

	// Sent all pending messages if any
	m_EventDispatcher.update();

	// Systems update
	for (SystemArray::iterator it = m_ActiveSystems.begin(); it != m_ActiveSystems.end(); it++)
	{
		(*it)->Update(m_Registry, m_DeltaTime);
	}
}

void GameWorld::PostUpdate()
{
	PROFILE_SCOPE();

	for (SystemArray::iterator it = m_ToBeEnabledSystems.begin(); it != m_ToBeEnabledSystems.end(); it++)
	{
		std::string t = typeid(**it).name();
		Debug::Log("Move system of type " + t + " from disabled to active");
		MoveSystem(*it, m_DisabledSystems, m_ActiveSystems);
	}
	m_ToBeEnabledSystems.clear();

	for (SystemArray::iterator it = m_ToBeDisabledSystems.begin(); it != m_ToBeDisabledSystems.end(); it++)
	{
		std::string t = typeid(**it).name();
		Debug::Log("Move system of type " + t + " from active to disabled");
		MoveSystem(*it, m_ActiveSystems, m_DisabledSystems);
	}
	m_ToBeDisabledSystems.clear();
}

void GameWorld::Exit()
{
	for (SystemArray::iterator it = m_ActiveSystems.begin(); it != m_ActiveSystems.end(); it++)
	{
		(*it)->Exit(m_Registry);
	}
	for (SystemArray::iterator it = m_DisabledSystems.begin(); it != m_DisabledSystems.end(); it++)
	{
		(*it)->Exit(m_Registry);
	}

	m_Registry.clear();
}

void GameWorld::SetupSystems(SystemArray systems)
{
	m_OriginalSystems.assign(systems.begin(), systems.end());
}

void GameWorld::EnableSystem(SystemBase* system, bool enabled)
{
	if (enabled)
	{
		m_ToBeEnabledSystems.push_back(system);
	}
	else
	{
		m_ToBeDisabledSystems.push_back(system);
	}
}

void GameWorld::MoveSystem(SystemBase* system, SystemArray& sourceArray, SystemArray& targetArray)
{
	auto findInSource = std::find(sourceArray.begin(), sourceArray.end(), system);
	assert(findInSource != sourceArray.end(), "Source system array doesn't contain the expected system");
	auto findInTarget = std::find(targetArray.begin(), targetArray.end(), system);
	assert(findInTarget == targetArray.end(), "Target system array contains the system");

	sourceArray.erase(findInSource);
	targetArray.push_back(system);

	// optional sorting here when needed
}

}
