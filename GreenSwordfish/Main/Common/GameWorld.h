#pragma once

#include "ECS.h"

namespace GS
{
class Game;
class SystemBase;

typedef std::function<void(Registry&)> AdditionalDataSetupFunc;

/**
 * Handles entt stuff and keeps SDL events separated.
 * Also it's nice to distinguish between the game world
 * and the game process.
 */
class GameWorld
{
	friend class Game;
	friend class Renderer;

public:
	GameWorld();
	~GameWorld();

public:

	template<typename EventType, typename ListenerType, void(ListenerType:: * member)(const EventType&)>
	void BindEvent(ListenerType* instance)
	{
		m_EventDispatcher.sink<EventType>().template connect<member>(instance);
	}

	template<typename EventType, typename ListenerType>
	void UnbindEvent(ListenerType* instance)
	{
		m_EventDispatcher.sink<EventType>().disconnect(instance);
	}

	template<typename EventType, typename... Args>
	void InvokeEvent(bool invokeImmediately, EventType&& e)
	{
		if (invokeImmediately)
			m_EventDispatcher.trigger(e);
		else
			m_EventDispatcher.enqueue(e);
	}

	inline Registry& GetRegistry() { return m_Registry; }

public:
	typedef std::vector<SystemBase*> SystemArray;
	void SetupSystems(SystemArray systems);

	void EnableSystem(class SystemBase* system, bool enabled);

	template<class SystemType>
	void EnableSystem(bool enabled)
	{
		static_assert(std::is_base_of<SystemBase, SystemType>::value, 
			"Requires SystemType to be derived from SystemBase");

		SystemArray::iterator findIt = std::find_if(m_ActiveSystems.begin(), m_ActiveSystems.end(), 
			[](SystemBase* system)
			{
				return dynamic_cast<SystemType*>(system) != nullptr;
			}
		);
		if (findIt != m_ActiveSystems.end())
		{
			EnableSystem(*findIt, enabled);
		}
	}

private:
	void MoveSystem(SystemBase* system, SystemArray& sourceArray, SystemArray& targetArray);

public:
	void Init(const AdditionalDataSetupFunc& dataSetupFunc);
	void Update();
	void PostUpdate();
	void Exit();

private:
	Registry m_Registry;
	EventDispatcher m_EventDispatcher;

	SystemArray m_OriginalSystems;

	SystemArray m_ActiveSystems;
	SystemArray m_DisabledSystems;
	SystemArray m_ToBeDisabledSystems;
	SystemArray m_ToBeEnabledSystems;

	std::chrono::steady_clock::time_point m_Timepoint;
	float m_DeltaTime;
};

}
