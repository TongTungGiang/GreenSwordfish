#include "WorldManager.h"

#include "GameWorld.h"
#include "Renderer.h"


#include "Systems/InputSystem.h"
#include "Systems/ClickSystem.h"
#include "Systems/MenuWorldInitialisationSystem.h"
#include "Systems/MapEditorInitialisationSystem.h"
#include "Systems/MainGameInitialisationSystem.h"
#include "Systems/UpdateMapBasedOnMouseMotionSystem.h"
#include "Systems/TileCursorFollowMouseSystem.h"
#include "Systems/DragSystem.h"
#include "Systems/UpdateMapSpriteSystem.h"
#include "Systems/CarSpawnSystem.h"
#include "Systems/CarMovementSystem.h"
#include "Systems/MoveForwardSystem.h"
#include "Systems/GeometrySortSystem.h"
#include "Systems/CarDestroySystem.h"
#include "Systems/JunctionClickSystem.h"
#include "Systems/CarDestinationSystem.h"
#include "Systems/UISortSystem.h"
#include "Systems/ScoreDisplaySystem.h"
#include "Systems/UpdateTextInputSystem.h"
#include "Systems/TimerSystem.h"
#include "Systems/GameResultResolveSystem.h"
#include "Systems/MapBatchRenderingSystem.h"

namespace GS
{



WorldManager::WorldManager() : m_CurrentWorld(nullptr)
{
	{
		GameWorld* menu = new GameWorld();
		menu->SetupSystems(
			{
				new InputSystem(menu),
				new MenuWorldInitialisationSystem(menu)
			});
		m_WorldHashMap.emplace(WorldName::Menu, menu);
	}

	{
		GameWorld* mapEditor = new GameWorld();
		mapEditor->SetupSystems(
			{
				new InputSystem(mapEditor),
				new DragSystem(mapEditor),
				//new ClickSystem(mapEditor),

				new MapEditorInitialisationSystem(mapEditor),

				new UpdateMapBasedOnMouseMotionSystem(mapEditor),
				new TileCursorFollowMouseSystem(mapEditor),
				new UpdateMapSpriteSystem(mapEditor),

				//new UpdateTextInputSystem(mapEditor),
			});
		m_WorldHashMap.emplace(WorldName::MapEditor, mapEditor);
	}

	{
		GameWorld* mainGame = new GameWorld();
		mainGame->SetupSystems(
			{
				new InputSystem(mainGame),
				//new ClickSystem(mainGame),

				new MainGameInitialisationSystem(mainGame),
				new UpdateMapSpriteSystem(mainGame),

				new CarSpawnSystem(mainGame),
				new CarMovementSystem(mainGame),
				new CarDestroySystem(mainGame),
				new MoveForwardSystem(mainGame),

				new GeometrySortSystem(mainGame),
				new UISortSystem(mainGame),

				new MapBatchRenderingSystem(mainGame),

				new JunctionClickSystem(mainGame),

				new CarDestinationSystem(mainGame),

				new ScoreDisplaySystem(mainGame),
				new TimerSystem(mainGame),

				new GameResultResolveSystem(mainGame),
			}
		);
		m_WorldHashMap.emplace(WorldName::MainGame, mainGame);
	}
}

WorldManager::~WorldManager()
{
	for (auto world : m_WorldHashMap)
	{
		delete world.second;
	}

	m_CurrentWorld = nullptr;
}

void WorldManager::Init()
{
	LoadWorldImmediate(WorldName::Menu);
}

void WorldManager::Exit()
{
	m_CurrentWorld->Exit();
}

void WorldManager::PostUpdate()
{
	m_CurrentWorld->PostUpdate();

	if (m_PostUpdateCommands.size() > 0)
	{
		for (auto commandIt = m_PostUpdateCommands.begin();
			commandIt != m_PostUpdateCommands.end();
			commandIt++)
		{
			(*commandIt)();
		}

		m_PostUpdateCommands.clear();
	}
}

void WorldManager::LoadWorldSafe(const char* worldName, const AdditionalDataSetupFunc& dataSetupFunc)
{
	m_PostUpdateCommands.push_back([this, worldName, dataSetupFunc]()
		{
			this->LoadWorldImmediate(worldName, dataSetupFunc);
		}
	);
}

void WorldManager::LoadWorldSafe(const std::string& worldName, const AdditionalDataSetupFunc& dataSetupFunc)
{
	m_PostUpdateCommands.push_back([this, &worldName, dataSetupFunc]()
		{
			this->LoadWorldImmediate(worldName, dataSetupFunc);
		}
	);
}

void WorldManager::LoadWorldImmediate(const std::string& worldName, const AdditionalDataSetupFunc& dataSetupFunc)
{
	LoadWorldImmediate(worldName.c_str(), dataSetupFunc);
}

void WorldManager::LoadWorldImmediate(const char* worldName, const AdditionalDataSetupFunc& dataSetupFunc)
{
	WorldHashMap::const_iterator find = m_WorldHashMap.find(worldName);
	if (find == m_WorldHashMap.end())
	{
		Debug::LogError("Cannot find the world named " + std::string(worldName));
	}
	else
	{
		Debug::Log("Loading world: " + std::string(worldName), Debug::DebugTextColor::Cyan);

		if (m_CurrentWorld != nullptr)
			m_CurrentWorld->Exit();

		m_CurrentWorld = find->second;
		m_CurrentWorld->Init(dataSetupFunc);

		OnWorldLoaded(m_CurrentWorld);
	}
}

}