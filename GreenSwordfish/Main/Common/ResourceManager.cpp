#include "ResourceManager.h"
#include "Renderer.h"

namespace GS
{

constexpr auto SPRITE_LIST_FILE = "Assets/AppConfig/SpriteList.txt";

ResourceManager::ResourceManager(SDL_Renderer* renderer) : m_Renderer(renderer) { }

ResourceManager::~ResourceManager()
{
}

void ResourceManager::Init()
{
	Debug::Log("Initialising Resource Manager", Debug::DebugTextColor::Yellow);

	ReadTextureConfig();
	ReadFontConfig();
}

void ResourceManager::Exit()
{
	for (auto it : m_TextureMap)
	{
		SDL_DestroyTexture(it.second.texture);
	}

	for (auto it : m_FontMap)
	{
		TTF_CloseFont(it.second);
	}
}

void ResourceManager::ReadTextureConfig()
{
	// Read texture config file
	std::stringstream jsonDocumentBuffer;
	{
		std::ifstream inputFile(SPRITE_LIST_FILE);
		std::string inputLine;
		while (std::getline(inputFile, inputLine))
		{
			jsonDocumentBuffer << inputLine << "\n";
		}
	}

	// Deserialise json content
	rapidjson::Document document;
	document.Parse(jsonDocumentBuffer.str().c_str());

	auto fileList = document.GetArray();
	for (rapidjson::Value::ValueIterator it = fileList.Begin(); it != fileList.End(); ++it)
	{
		rapidjson::Value& attribute = *it;
		std::string filePath = attribute.GetString();

		// Read texture file
		SDL_Texture* texture = LoadTexture(filePath + ".png");
		if (texture == NULL)
		{
			continue;
		}

		// Read sheet info in the associated xml file
		if (std::filesystem::exists(filePath + ".xml"))
		{
			Debug::Log("Read additional sheet info in " + filePath + ".xml");
			rapidxml::xml_document<> xmlDoc;
			rapidxml::file<> xmlFile((filePath + ".xml").c_str());
			xmlDoc.parse<0>(xmlFile.data());

			for (rapidxml::xml_node<>* node = xmlDoc.first_node()->first_node();
				node != NULL;
				node = node->next_sibling())
			{
				std::string name = std::string(node->first_attribute("name")->value());
				int x = atoi(node->first_attribute("x")->value());
				int y = atoi(node->first_attribute("y")->value());
				int width = atoi(node->first_attribute("width")->value());
				int height = atoi(node->first_attribute("height")->value());
				m_TextureMap[name] = { texture, Rect(x, y, width, height) };
			}
		}
		else
		{
			Debug::Log("No " + filePath + ".xml, skipping...");

			std::size_t lastSlash = filePath.find_last_of("/\\");
			std::string name = filePath.substr(lastSlash + 1);
			name += ".png";
			int width, height;
			SDL_QueryTexture(texture, NULL, NULL, &width, &height);
			m_TextureMap[name] = { texture, Rect(0, 0, width, height) };
		}
	}
}

void ResourceManager::ReadFontConfig()
{
	std::string fileName = "Assets/Fonts/Arial.ttf";
	TTF_Font* font = TTF_OpenFont(fileName.c_str(), 50);
	m_FontMap["Arial"] = font;
}

SDL_Texture* ResourceManager::LoadTexture(const std::string& path)
{
	SDL_Texture* loadedTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		Debug::LogError("Unable to load image " + path + ", error: " + IMG_GetError());
	}
	else
	{
		loadedTexture = SDL_CreateTextureFromSurface(m_Renderer, loadedSurface);
		if (loadedTexture == NULL)
		{
			Debug::LogError("Unable to create texture from " + path + ", error " + SDL_GetError());
		}

		SDL_FreeSurface(loadedSurface);
	}

	if (loadedTexture == NULL)
		return NULL;

	return loadedTexture;
}

Sprite* ResourceManager::GetTexture(const std::string& path)
{
	auto search = m_TextureMap.find(path);
	if (search != m_TextureMap.end())
		return &m_TextureMap[path];

	return NULL;
}

TTF_Font* ResourceManager::GetFont(const std::string& id)
{
	auto search = m_FontMap.find(id);
	if (search != m_FontMap.end())
		return m_FontMap[id];

	return NULL;
}

}