#pragma once

#include "ECS.h"
#include "ISystem.h"
#include "Renderer.h"
#include "ResourceManager.h"

namespace GS
{
class RenderingSystemBase : public ISystemBase
{
public:
	RenderingSystemBase(Renderer* renderer) : m_Renderer(renderer) {}
	virtual ~RenderingSystemBase() { }
	virtual void Init(Registry& registry) { }
	virtual void Update(Registry& registry) = 0;
	virtual void Exit(Registry& registry) { }

protected:
	Renderer* m_Renderer;
};
}
