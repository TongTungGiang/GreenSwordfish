#pragma once

#include <string>
#include <vector>
#include <array>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <map>
#include <set>
#include <filesystem>

#include <iostream>
#include <fstream>
#include <sstream>

#include <chrono>

#include <cstdlib>
#include <cmath>

#include <functional>

#include <random>
#include <algorithm>
#include <execution>

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "sdlgui/nanogui.h"

#include "entt/entt.hpp"

#include "RapidJSON/document.h"
#include "RapidJSON/istreamwrapper.h"
#include "RapidJSON/ostreamwrapper.h"
#include "RapidJSON/prettywriter.h"

#include "RapidXML/rapidxml.hpp"
#include "RapidXML/rapidxml_utils.hpp"

#include "CompileConfig.h"

#include "Utilities/Debug.h"
#include "Utilities/Profiler.h"

#include "Math/Math.h"

