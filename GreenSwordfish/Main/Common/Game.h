#pragma once

#include "Settings.h"
#include "GUI/GUIScreen.h"

namespace GS
{

class ResourceManager;
class WorldManager;
class Renderer;

/**
 * Intentionally, the Game class handles SDL events
 */
class Game
{
public:
	void Init();
	void Run();

public:
	WorldManager* GetWorldManager();
	GUIScreen* GetGUI();
	SDL_Renderer* GetGlobalSDLRenderer() { return m_SdlRenderer; }
	ResourceManager* GetResourceManager();

private:
	void ResetRenderer(class GameWorld* world);

private:
	std::unique_ptr<ResourceManager> m_ResourceManager;
	std::unique_ptr<WorldManager> m_WorldManager;
	std::unique_ptr<Renderer> m_Renderer;

	SDL_Window* m_Window;
	SDL_Renderer* m_SdlRenderer;
	std::unique_ptr<GUIScreen> m_GuiWindow;

	// Singleton
public:
	static Game* Get();

private:
	static Game* s_Instance;
	Game();
	~Game();
};

}
