#pragma once

#include "Prerequisites.h"

// App settings
constexpr auto WINDOWS_TITLE = "Game";
static const int WIDTH = 800;
static const int HEIGHT = 600;
static const Uint32 MODE = SDL_WINDOW_SHOWN;

// Game settings

