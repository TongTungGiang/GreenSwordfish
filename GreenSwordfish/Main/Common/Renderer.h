#pragma once

#include "Settings.h"
#include "ECS.h"

namespace GS
{
class GameWorld;
class ResourceManager;

class Renderer
{
public:

	Renderer(class ResourceManager* resourceManager, SDL_Renderer* renderer);
	~Renderer();

	void Init();
	void Render();
	void Exit();
	inline void SetupSystems();

	SDL_Renderer* GetSdlRenderer() { return m_SdlRenderer; }
	class ResourceManager* GetResourceManager() { return m_ResourceManager; }

	void SetupWorld(class GameWorld* world);

private:
	SDL_Renderer* m_SdlRenderer;

	class ResourceManager* m_ResourceManager;

	class GameWorld* m_World;

	typedef std::vector<class RenderingSystemBase*> SystemArray;
	SystemArray m_RenderingSystems;

};
}


