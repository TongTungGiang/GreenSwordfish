#pragma once

#include "Prerequisites.h"

namespace GS
{

typedef entt::registry Registry;
typedef Registry::entity_type Entity;
typedef entt::dispatcher EventDispatcher;

static const Entity NULL_ENTITY = entt::null;

}
