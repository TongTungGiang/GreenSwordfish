#include "Game.h"
#include "SDL.h"

#include "GUI/GUIScreen.h"

#include "WorldManager.h"
#include "ResourceManager.h"

#include "Systems/InputSystem.h"

namespace GS
{

Game* Game::s_Instance = nullptr;

Game* Game::Get()
{
	if (s_Instance == nullptr)
		s_Instance = new Game();

	return s_Instance;
}

Game::Game()
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	m_Window = SDL_CreateWindow(WINDOWS_TITLE,
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		WIDTH, HEIGHT,
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);

	auto context = SDL_GL_CreateContext(m_Window);

	char rendername[256] = { 0 };
	SDL_RendererInfo info;
	for (int it = 0; it < SDL_GetNumRenderDrivers(); it++) {
		SDL_GetRenderDriverInfo(it, &info);
		strcat(rendername, info.name);
		strcat(rendername, " ");
	}

	m_SdlRenderer = SDL_CreateRenderer(m_Window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawBlendMode(m_SdlRenderer, SDL_BLENDMODE_BLEND);

	TTF_Init();
	
	m_ResourceManager = std::make_unique<ResourceManager>(m_SdlRenderer);
	m_WorldManager = std::make_unique<WorldManager>();
	m_Renderer = std::make_unique<Renderer>(m_ResourceManager.get(), m_SdlRenderer);

	m_GuiWindow = std::make_unique<GUIScreen>(m_Window, WINDOWS_TITLE, WIDTH, HEIGHT);
}

Game::~Game()
{
	TTF_Quit();

	SDL_DestroyWindow(m_Window);
	SDL_Quit();
}

void Game::Init()
{
	m_WorldManager->OnWorldLoaded = [this](GameWorld* world)
	{
		this->m_Renderer->Exit();
		this->m_Renderer->SetupWorld(world);
		this->m_Renderer->Init();
	};

	m_ResourceManager->Init();
	m_Renderer->Init();
	m_WorldManager->Init();
}

void Game::Run()
{
	bool done = false;
	std::string inputText;
	SDL_StartTextInput();
	while (!done)
	{
		PROFILE_FRAME("Game::Run");

		SDL_Event event;

		while (SDL_PollEvent((&event)))
		{
			Registry& registry = m_WorldManager->GetCurrentWorld()->m_Registry;
			switch (event.type)
			{
			case SDL_QUIT:
				done = true;
				break;

			default:
				InputSystem::HandleSDLEvent(registry, event);
				m_GuiWindow->onEvent(event);
			}
		}

		m_WorldManager->GetCurrentWorld()->Update();
		m_Renderer->Render();

		m_WorldManager->PostUpdate();
	}

	m_WorldManager->GetCurrentWorld()->Exit();
	m_Renderer->Exit();
	m_ResourceManager->Exit();
}

WorldManager* Game::GetWorldManager()
{
	return m_WorldManager.get();
}

GS::GUIScreen* Game::GetGUI()
{
	return m_GuiWindow.get();
}

GS::ResourceManager* Game::GetResourceManager()
{
	return m_ResourceManager.get();
}

void Game::ResetRenderer(GameWorld* world)
{
	m_Renderer->Exit();
	m_Renderer->SetupWorld(world);
	m_Renderer->Init();
}

}
