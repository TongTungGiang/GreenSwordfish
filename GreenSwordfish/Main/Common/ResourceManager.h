#pragma once

#include "Settings.h"

namespace GS
{

struct Sprite
{
	SDL_Texture* texture;
	Rect clip;
};

class ResourceManager
{

public:
	ResourceManager(SDL_Renderer* renderer);
	~ResourceManager();

public:
	void Init();
	void Exit();

private:
	void ReadTextureConfig();
	void ReadFontConfig();

private:
	SDL_Texture* LoadTexture(const std::string& path);

public:
	Sprite* GetTexture(const std::string& id);
	TTF_Font* GetFont(const std::string& id);

private:

	typedef std::unordered_map<std::string, Sprite> TextureMap;
	TextureMap m_TextureMap;

	typedef std::unordered_map <std::string, TTF_Font*> FontMap;
	FontMap m_FontMap;

	SDL_Renderer* m_Renderer;

};

}
