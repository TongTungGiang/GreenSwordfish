#pragma once

namespace GS
{
class GUIScreen : public sdlgui::Screen
{
public:
	GUIScreen(SDL_Window* window, const std::string& appTitle, int width, int height);
	virtual ~GUIScreen();

public:
	void reset();

	virtual bool keyboardEvent(int key, int scancode, int action, int modifiers) override;
	virtual void draw(SDL_Renderer* renderer) override;
	virtual void drawContents() override;
	virtual void drawAll() override;
	virtual bool onEvent(SDL_Event& event) override;

	inline sdlgui::Window& GetGuiWindow();

private:
	void createDefaultWindow();

};
}
