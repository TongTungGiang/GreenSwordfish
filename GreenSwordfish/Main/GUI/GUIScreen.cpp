#include "GUIScreen.h"

using namespace sdlgui;
using std::cout;
using std::endl;
using sdlgui::Color;
using sdlgui::Vector2i;

namespace GS
{

GUIScreen::GUIScreen(SDL_Window* sdlWindow, const std::string& appTitle, int width, int height) :
	sdlgui::Screen(sdlWindow, sdlgui::Vector2i(width, height), appTitle)
{
	createDefaultWindow();
}

GUIScreen::~GUIScreen() { }

void GUIScreen::reset()
{
	if (mChildren.size() == 0)
		return;

	Window& prevDefaultWindow = GetGuiWindow();
	prevDefaultWindow.dispose();
	createDefaultWindow();
}

bool GUIScreen::keyboardEvent(int key, int scancode, int action, int modifiers)
{
	if (Screen::keyboardEvent(key, scancode, action, modifiers))
		return true;

	return false;
}

void GUIScreen::draw(SDL_Renderer* renderer)
{
	Screen::draw(renderer);
}

void GUIScreen::drawContents() { }

void GUIScreen::drawAll()
{
	PROFILE_SCOPE();
	Screen::drawAll();
}

bool GUIScreen::onEvent(SDL_Event& event)
{
	PROFILE_SCOPE();
	return Screen::onEvent(event);
}

sdlgui::Window& GUIScreen::GetGuiWindow()
{
	sdlgui::Window* windowPtr = (*(mChildren.begin()))->cast<sdlgui::Window>();
	return *windowPtr;
}

void GUIScreen::createDefaultWindow()
{
	sdlgui::Window& defaultWindow = window("");
	defaultWindow.setPosition({ 0, 0 });
	defaultWindow.setFixedSize({ width(), height() });
	
	Theme* defaultTheme = defaultWindow.theme();
	sdlgui::Color transparent = { 0, 0, 0, 0 };
	defaultTheme->mWindowFillFocused = transparent;
	defaultTheme->mWindowFillUnfocused = transparent;
	defaultTheme->mBorderDark = transparent;
	defaultTheme->mWindowDropShadowSize = 0;
	defaultTheme->mWindowHeaderHeight = 0;
}

}