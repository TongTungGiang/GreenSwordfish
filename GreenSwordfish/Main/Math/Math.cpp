#include "Math.h"
#include "Settings.h"

#include "Data/MapData.h"

namespace GS::Math
{

bool IsInRect(Rect rect, Vector2 point)
{
	Vector2 min = rect.GetCoord();
	Vector2 max = rect.GetCoord() + rect.GetSize();
	return min.x <= point.x && point.x <= max.x &&
		min.y <= point.y && point.y <= max.y;
}

Vector2 WorldToIso(Vector2 world)
{
	return Vector2(world.x - world.y, (world.x + world.y) / 2);
}

Vector2 IsoToWorld(Vector2 iso)
{
	return Vector2((2 * iso.y + iso.x) / 2, (2 * iso.y - iso.x) / 2);

}

Vector2 IsoToTileCoordinate(Vector2 iso)
{
	// Normalise iso coordinate
	iso.x -= TILE_SPRITE_WIDTH_BLANK / 2;
	iso.y -= TILE_SPRITE_HEIGHT_BLANK / 2;

	Vector2 map;
	map.x = ((2 * iso.x / TILE_SPRITE_WIDTH_BLANK) + (2 * iso.y / TILE_SPRITE_HEIGHT_BLANK)) / 2;
	map.x = std::round(map.x);
	map.y = ((2 * iso.y / TILE_SPRITE_HEIGHT_BLANK) - (2 * iso.x / TILE_SPRITE_WIDTH_BLANK)) / 2;
	map.y = std::round(map.y);

	return map;
}

GS::Vector2 TileCoordinateToWorld(int x, int y)
{ 
	return Vector2(x * TILE_WORLD_SIZE, y * TILE_WORLD_SIZE);
}

GS::Vector2 WorldToTileCoordinate(Vector2 world)
{
	int x = (int)std::floor(world.x / TILE_WORLD_SIZE);
	int y = (int)std::floor(world.y / TILE_WORLD_SIZE);
	return Vector2(x, y);
}

float Dot(Vector2 a, Vector2 b)
{
	return a.x * b.x + a.y * b.y;
}

float Magnitude(Vector2 a)
{
	return std::sqrt(a.x * a.x + a.y * a.y);
}

Vector2 Normalize(Vector2 a)
{
	float magnitude = Magnitude(a);
	return Vector2(a.x / magnitude, a.y / magnitude);
}

float Abs(float value)
{
	return value > 0 ? value : value * -1;
}

int Abs(int value)
{
	return value > 0 ? value : value * -1;
}

}
