#pragma once

namespace GS
{

struct Vector2
{
	float x, y;

	Vector2(float x = 0, float y = 0) : x(x), y(y) {}

	Vector2 operator + (const Vector2& other) { return Vector2(x + other.x, y + other.y); }
	Vector2& operator += (const Vector2& other) { x += other.x; y += other.y; return *this; }
	Vector2 operator - (const Vector2& other) { return Vector2(x - other.x, y - other.y); }
	Vector2& operator -= (const Vector2& other) { x -= other.x; y -= other.y; return *this; }
	Vector2 operator *(const float k) const { return Vector2(x * k, y * k); }
	Vector2& operator *= (const float k) { x *= k; y *= k; return *this; }
	Vector2 operator /(const float k) const { return Vector2(x / k, y / k); }
	Vector2& operator /= (const float k) { x /= k; y /= k; return *this; }

	friend bool operator == (const Vector2& a, const Vector2& b);
	friend bool operator != (const Vector2& a, const Vector2& b);

	const static Vector2 zero;
	const static Vector2 east;
	const static Vector2 west;
	const static Vector2 north;
	const static Vector2 south;
};

std::ostream& operator<< (std::ostream& out, const Vector2& v);

}
