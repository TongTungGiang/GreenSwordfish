#pragma once

namespace GS
{
struct Color
{
	unsigned char r, g, b, a;

	Color(unsigned char r = 0, unsigned char g = 0, unsigned char b = 0, unsigned char a = 0) : r(r), g(g), b(b), a(a) { }

	const static Color black;
	const static Color white;
	const static Color red;
	const static Color green;
	const static Color blue;
};
}
