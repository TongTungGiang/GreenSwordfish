#pragma once

#include "Vector2.h"
#include "Rect.h"
#include "Color.h"

namespace GS
{
namespace Math
{

// Vector functions
float Dot(Vector2 a, Vector2 b);
float Magnitude(Vector2 a);
Vector2 Normalize(Vector2 a);

// Rect functions
bool IsInRect(Rect rect, Vector2 point);

// Isometric functions
Vector2 IsoToTileCoordinate(Vector2 iso);
Vector2 IsoToWorld(Vector2 iso);
Vector2 WorldToIso(Vector2 world);

Vector2 TileCoordinateToWorld(int x, int y);
Vector2 WorldToTileCoordinate(Vector2 world);

// Arithmetic functions
float Abs(float value);
int Abs(int value);

}
}
