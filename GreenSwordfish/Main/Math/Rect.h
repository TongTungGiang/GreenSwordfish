#pragma once

#include "Vector2.h"

namespace GS
{
struct Rect
{
	float x, y, width, height;

	Rect(float x = 0, float y = 0, float width = 0, float height = 0) : x(x), y(y), width(width), height(height) {}
	Rect(Vector2 coord, Vector2 size) : x(coord.x), y(coord.y), width(size.x), height(size.y) {}

	Vector2 GetCoord() const { return Vector2(x, y); }
	Vector2 GetSize() const { return Vector2(width, height); }

	void SetCoord(Vector2 coord) { x = coord.x, y = coord.y; }
	void SetSize(Vector2 size) { width = size.x, height = size.y; }

	SDL_Rect ToSDLRect() const
	{
		return { static_cast<int>(x), static_cast<int>(y), static_cast<int>(width), static_cast<int>(height) };
	}
};
}
