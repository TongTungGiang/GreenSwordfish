#include "Vector2.h"

namespace GS
{

bool operator == (const Vector2 & a, const Vector2 & b)
{
	return a.x == b.x && a.y == b.y;
}

bool operator != (const Vector2 & a, const Vector2 & b)
{
	return a.x != b.x || a.y != b.y;
}

const Vector2 Vector2::zero = Vector2(0, 0);
const Vector2 Vector2::east = Vector2(1, 0);
const Vector2 Vector2::west = Vector2(-1, 0);
const Vector2 Vector2::north = Vector2(0, 1);
const Vector2 Vector2::south = Vector2(0, -1);

std::ostream& operator<<(std::ostream& out, const Vector2& v)
{
	out << "(" << v.x << "," << v.y << ")";
	return out;
}

}