#pragma once

#include "ECS.h"

namespace GS
{
struct ScoreData
{
	int currentScore;
};

struct ScoreDisplayHUDData
{
	Entity scoreTextEntity;
};
}