#pragma once

#include "ECS.h"

namespace GS
{
struct CurrentlyEditingText { Entity entity; };

typedef std::unordered_map<std::string, Entity> InputTextPurposeDict;

}

namespace GS::InputHelpers
{
void MarkEntityAsCurrentlyEditingText(Registry& registry, Entity entity);
std::string GetInputValue(Registry& registry, const std::string& purposeKey);
}

namespace GS::InputPurpose
{
constexpr auto MAP_NAME = "mapname";
constexpr auto MAP_TARGET_SCORE = "maptargetscore";
constexpr auto MAP_PLAY_TIME = "mapplaytime";
}