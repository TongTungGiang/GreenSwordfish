#include "MapData.h"
#include "Math/Vector2.h"

namespace GS
{

std::unordered_map<TileType, std::string> g_TileSprites =
{
	{ TileType::Blank, "blankTile.png" },
	//{ TileType::Blank, "" },

	{ TileType::BlankRoad, "cityTiles_066.png" },

	{ TileType::HorizontalRoad, "cityTiles_073.png" },
	{ TileType::VerticalRoad, "cityTiles_081.png" },

	{ TileType::JunctionX, "cityTiles_089.png" },

	{ TileType::JunctionT_E, "cityTiles_095.png" },
	{ TileType::JunctionT_W, "cityTiles_088.png" },
	{ TileType::JunctionT_N, "cityTiles_096.png" },
	{ TileType::JunctionT_S, "cityTiles_103.png" },

	{ TileType::JunctionL_NE, "cityTiles_122.png" },
	{ TileType::JunctionL_NW, "cityTiles_125.png" },
	{ TileType::JunctionL_SE, "cityTiles_124.png" },
	{ TileType::JunctionL_SW, "cityTiles_126.png" },

	{ TileType::EndE, "cityTiles_111.png" },
	{ TileType::EndW, "cityTiles_104.png" },
	{ TileType::EndN, "cityTiles_116.png" },
	{ TileType::EndS, "cityTiles_110.png" },

	{ TileType::BlankGrass, "grassTile.png" },

	{ TileType::Building1, "abstractTile_01.png" },
	{ TileType::Building2, "abstractTile_05.png" },
	{ TileType::Building3, "abstractTile_09.png" },
	{ TileType::Building4, "abstractTile_13.png" },

	{ TileType::StartPoint, "cityTiles_066.png" },
};

int GetTileIndex(int x, int y)
{
	assert(IsTileCoordinateValid(x, y));
	int normalisedY = y + DEFAULT_MAP_SIZE / 2;
	return x * DEFAULT_MAP_SIZE + normalisedY;
}

int GetTileIndex(Vector2 coordinate)
{
	return GetTileIndex((int)coordinate.x, (int)coordinate.y);
}

Vector2 GetTileCoordinate(int index)
{
	assert(0 <= index && index < DEFAULT_MAP_SIZE* DEFAULT_MAP_SIZE);
	int x = index / DEFAULT_MAP_SIZE;
	int normalisedY = index % DEFAULT_MAP_SIZE;
	int y = normalisedY - DEFAULT_MAP_SIZE / 2;
	return Vector2(x, y);
}

bool IsTileCoordinateValid(int x, int y)
{
	PROFILE_SCOPE();
	return 0 <= x && x < DEFAULT_MAP_SIZE && -DEFAULT_MAP_SIZE / 2 <= y && y < DEFAULT_MAP_SIZE / 2;
}

bool IsTileCoordinateValid(Vector2 coordinate)
{
	PROFILE_SCOPE();
	int x = (int)coordinate.x; 
	int y = (int)coordinate.y;
	return IsTileCoordinateValid(x, y);
}

bool IsTileCoordinateValid(const MapTileArray& map, Vector2 coordinate)
{
	int index = GetTileIndex(coordinate);
	return map.find(index) != map.end();
}

bool IsTileCoordinateValid(const MapTileArray& map, int x, int y)
{
	int index = GetTileIndex(x, y);
	return map.find(index) != map.end();
}

std::string GetArrowSpriteID(Vector2 direction, TileType type)
{
	std::stringstream spriteName;
	spriteName << "arrow";

	if (Math::Dot(direction, Vector2::east) > 0.5f)
		spriteName << "East";
	else if (Math::Dot(direction, Vector2::west) > 0.5f)
		spriteName << "West";
	else if (Math::Dot(direction, Vector2::north) > 0.5f)
		spriteName << "North";
	else if (Math::Dot(direction, Vector2::south) > 0.5f)
		spriteName << "South";

	if (type == TileType::JunctionT_E ||
		type == TileType::JunctionT_W ||
		type == TileType::JunctionT_N ||
		type == TileType::JunctionT_S ||
		type == TileType::JunctionX)
		spriteName << "L";

	spriteName << ".png";
	return spriteName.str();
}

bool IsBuilding(TileType type)
{
	return type >= TileType::Building1 && type <= TileType::Building4;
}

bool IsJunction(TileType type)
{
	switch (type)
	{
	case GS::TileType::JunctionX:
	case GS::TileType::JunctionT_N:
	case GS::TileType::JunctionT_E:
	case GS::TileType::JunctionT_W:
	case GS::TileType::JunctionT_S:
		return true;
	default:
		return false;
	}
}

}
