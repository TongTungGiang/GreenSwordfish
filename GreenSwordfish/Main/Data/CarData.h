#pragma once

namespace GS
{
constexpr int CAR_TYPE_COUNT = 5;
constexpr float CAR_MOVE_SPEED = 25;
constexpr float CAR_SPAWN_INTERVAL = 4.0f;

std::string GetCarSprite(int type, Vector2 direction);
}
