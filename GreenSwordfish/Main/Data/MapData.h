#pragma once

#include "Settings.h"

namespace GS
{

// Dimension settings
constexpr int DEFAULT_MAP_SIZE = 50;
constexpr int TILE_WORLD_SIZE = 32;
constexpr int TILE_SPRITE_WIDTH_BLANK = 64;
constexpr int TILE_SPRITE_HEIGHT_BLANK = 32;
constexpr int TILE_SPRITE_WIDTH = 66;
constexpr int TILE_SPRITE_HEIGHT = 50;
constexpr int BUILDING_SPRITE_WIDTH = 64;
constexpr int BUILDING_SPRITE_HEIGHT = BUILDING_SPRITE_WIDTH;

// Helper types
enum class TileType
{
	Blank,

	BlankRoad,

	HorizontalRoad, VerticalRoad,

	JunctionX,

	JunctionT_N, JunctionT_E, JunctionT_W, JunctionT_S,

	JunctionL_NW, JunctionL_NE, JunctionL_SW, JunctionL_SE,

	EndN, EndE, EndW, EndS,

	Building1, Building2, Building3, Building4,

	StartPoint,

	BlankGrass,
};

extern std::unordered_map<TileType, std::string> g_TileSprites;

static constexpr size_t MAP_ARRAY_SIZE = DEFAULT_MAP_SIZE * DEFAULT_MAP_SIZE;

struct MapTileInfo
{
	TileType type;
	Vector2 direction;
	bool isDirty;
};
typedef std::unordered_map<int, MapTileInfo> MapTileArray;

struct MapGameplayInfo
{
	int targetScore;
	int playTime;
};

struct MapLoadInfo
{
	std::string loadedMap;
};

// Helper functions
int GetTileIndex(int x, int y);
int GetTileIndex(Vector2 coordinate);
struct Vector2 GetTileCoordinate(int index);
bool IsTileCoordinateValid(int x, int y);
[[deprecated]] bool IsTileCoordinateValid(Vector2 coordinate);
bool IsTileCoordinateValid(const MapTileArray& map, Vector2 coordinate);
bool IsTileCoordinateValid(const MapTileArray& map, int x, int y);

std::string GetArrowSpriteID(Vector2 direction, TileType type);

bool IsBuilding(TileType type);
bool IsJunction(TileType type);

}
