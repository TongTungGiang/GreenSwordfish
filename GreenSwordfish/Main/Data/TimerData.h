#pragma once

#include "ECS.h"

namespace GS
{
struct TimerData
{
	float elapsedTime;
};

struct TimerDisplayHUDData
{
	Entity timerTextEntity;
};
}
