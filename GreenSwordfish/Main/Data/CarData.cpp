#include "CarData.h"

namespace GS
{

std::string GetCarSprite(int type, Vector2 direction)
{
	std::string directionStr = "_N";
	if (Math::Dot(direction, Vector2::east) > 0.5f)
	{
		directionStr = "_E";
	}
	else if (Math::Dot(direction, Vector2::west) > 0.5f)
	{
		directionStr = "_W";
	}
	else if (Math::Dot(direction, Vector2::north) > 0.5f)
	{
		directionStr = "_N";
	}
	else if (Math::Dot(direction, Vector2::south) > 0.5f)
	{
		directionStr = "_S";
	}
	
	return "car" + std::to_string(type) + directionStr + ".png";
}

}

