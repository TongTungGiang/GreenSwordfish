#pragma once

namespace GS
{
struct DragData
{
	struct Vector2 startPosition;
	struct Vector2 currentPosition;
};
}
