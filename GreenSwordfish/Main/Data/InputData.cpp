#include "InputData.h"

#include "Components/TextComponent.h"
#include "Components/InputDataComponent.h"

#include "Systems/InputSystem.h"

namespace GS::InputHelpers
{

void MarkEntityAsCurrentlyEditingText(Registry& registry, Entity entity)
{
	assert(registry.has<TextComponent>(entity));

	CurrentlyEditingText& currentlyEditingTextData = registry.ctx<CurrentlyEditingText>();
	currentlyEditingTextData.entity = entity;

	TextComponent newText = registry.get<TextComponent>(entity);

	TextInputDataComponent& textInput = registry.get<TextInputDataComponent>(InputSystem::GetInputEntity(registry));
	textInput.value = newText.value;

	Debug::Log("Marked entity " + std::to_string((int)entity) + " as currently editing text");
}

std::string GetInputValue(Registry& registry, const std::string& purposeKey)
{
	const auto& inputDict = registry.ctx<InputTextPurposeDict>();
	if (inputDict.find(purposeKey) == inputDict.end())
	{
		Debug::Log("Cannot find input key: " + purposeKey);
		return "";
	}

	Entity inputEntity = inputDict.at(purposeKey);
	assert(registry.has<TextComponent>(inputEntity));

	const TextComponent& textField = registry.get<TextComponent>(inputEntity);
	return textField.value;
}

}