#pragma once

#include "ECS.h"

namespace GS
{
struct CarInformationRelationshipComponent
{
	Entity car;
	Entity information;
	Entity calloutBackground;

	CarInformationRelationshipComponent(Entity car, Entity information, Entity calloutBackground)
		: car(car), information(information), calloutBackground(calloutBackground)
	{
	}
};
}
