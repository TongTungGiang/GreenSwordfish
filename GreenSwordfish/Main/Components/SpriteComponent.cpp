#include "SpriteComponent.h"
#include "Game.h"
#include "ResourceManager.h"

namespace GS
{

void SpriteComponent::SetTexture(const std::string& textureID)
{
	auto sprite = Game::Get()->GetResourceManager()->GetTexture(textureID);
	if (sprite != NULL)
	{
		m_Texture = sprite->texture;
		m_Clip = sprite->clip;
	}
}

}