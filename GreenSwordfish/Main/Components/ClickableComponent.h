#pragma once

#include "ECS.h"

namespace GS
{

typedef void(*OnClickFunc)(Registry&, Entity);

struct ClickableComponent
{
	OnClickFunc onClick;

	ClickableComponent(OnClickFunc onClick = nullptr) : onClick(onClick) { }
};

}
