#pragma once

namespace GS
{
enum class UISortLayer
{
	Callout,
	Destination,

	HUD
};

struct UISortLayerComponent
{
	int value;

	UISortLayerComponent(int sortLayer) : value(sortLayer) {}
	UISortLayerComponent(UISortLayer sortLayer) : value((int)sortLayer) {}
};
}
