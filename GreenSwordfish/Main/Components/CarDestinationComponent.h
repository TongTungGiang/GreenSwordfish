#pragma once
#include "Data/MapData.h"

namespace GS
{
struct CarDestinationComponent
{
	TileType value;

	CarDestinationComponent(TileType destinationType) : value(destinationType)
	{
		assert(destinationType <= TileType::Building4 && destinationType >= TileType::Building1);
	}
};
}
