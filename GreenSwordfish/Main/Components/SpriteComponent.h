#pragma once

#include "Settings.h"
#include "Math/Vector2.h"
#include "Math/Rect.h"

namespace GS
{

struct SpriteComponent
{
	Rect renderRect;

	void SetTexture(const std::string& textureID);
	inline SDL_Texture* GetTexture() const { return m_Texture; }
	inline Rect GetClipRect() const { return m_Clip; }

	SpriteComponent(Rect renderRect) : renderRect(renderRect), m_Texture(NULL) {}
	SpriteComponent(Rect renderRect, SDL_Texture* sdlTexture, Rect clipRect) : renderRect(renderRect), m_Texture(sdlTexture), m_Clip(clipRect) {}

private:
	SDL_Texture* m_Texture;
	Rect m_Clip;
};

struct BatchRenderingSpriteComponent {};
struct BakedSpriteComponent {};

}

