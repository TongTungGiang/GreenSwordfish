#pragma once

namespace GS
{
struct CarTypeComponent
{
	int value;

	CarTypeComponent(int type) : value(type) { }
};
}
