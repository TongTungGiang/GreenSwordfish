#pragma once

namespace GS
{
struct TransformComponent
{
	Vector2 direction;
	Vector2 position;
	float height;
	int layer;

	TransformComponent(Vector2 direction, Vector2 position, float height, int layer) : direction(direction), position(position), height(height), layer(layer) {}
};

namespace Layers
{
constexpr int GROUND = 0;
constexpr int GROUND_OVERLAY = GROUND + 1;
constexpr int PROPS = GROUND_OVERLAY + 1;
}
}
