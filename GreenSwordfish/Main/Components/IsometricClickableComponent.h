#pragma once

#include "ECS.h"

namespace GS
{

typedef void(*OnClickFunc)(Registry&, Entity);

struct IsometricClickableComponent
{
	OnClickFunc onClick;

	IsometricClickableComponent(OnClickFunc onClick = nullptr) : onClick(onClick) { }
};
}
