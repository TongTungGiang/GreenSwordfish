#pragma once

namespace GS
{
struct MoveForwardComponent
{
	Vector2 target;
	float speed;

	MoveForwardComponent(Vector2 target, float speed) : target(target), speed(speed) {}
};
}
