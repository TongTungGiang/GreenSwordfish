#pragma once

#include "Data/MapData.h"

namespace GS
{
struct TileComponent
{
	int x, y;
	TileComponent(int x, int y) : x(x), y(y) { }
};

struct TileTypeComponent
{
	TileType value;

	TileTypeComponent() : value(TileType::BlankRoad) { }
};
}
