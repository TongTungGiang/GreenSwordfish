#pragma once

namespace GS
{
struct ArrowComponent 
{
	Vector2 direction;

	ArrowComponent() : direction(Vector2::zero) { }
	ArrowComponent(Vector2 direction) : direction(direction) { }
};
}
