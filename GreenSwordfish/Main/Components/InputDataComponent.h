#pragma once

#include "Math/Vector2.h"

namespace GS
{


enum class InputState
{
	Up,
	JustDown,
	Down,
	JustUp
};

struct InputDataComponent
{
	// Processed data
	InputState leftButtonState;
	InputState rightButtonState;

	// Raw data fetch from SDL
	Vector2 mouse;
	bool leftButton;
	bool rightButton;

	InputDataComponent() :
		leftButtonState(InputState::Up), rightButtonState(InputState::Up),
		mouse(Vector2::zero), leftButton(false), rightButton(false)
	{
	}
};

struct TextInputDataComponent
{
	std::string value;
};

}
