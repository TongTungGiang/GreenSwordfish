#pragma once

namespace GS
{
struct TextComponent 
{
	std::string value;
	Rect renderRect;
	Color color;

	TextComponent(const std::string& value, Rect rect, Color color) : TextComponent(value.c_str(), rect, color) { }
	TextComponent(const char* value, Rect rect, Color color) : value(value), renderRect(rect), color(color) { }
};
}
