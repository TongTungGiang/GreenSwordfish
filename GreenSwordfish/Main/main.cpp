#include "Game.h"

using namespace GS;

int main(int argc, char* argv[])
{
	Game::Get()->Init();
	Game::Get()->Run();

	return 0;
}
