#pragma once

#include <string>

namespace GS
{
namespace Debug
{


enum class DebugTextColor
{
	Red,
	Green,
	Yellow,
	Blue,
	Magenta,
	Cyan,
	White
};


void Log(const std::string& content, DebugTextColor color = DebugTextColor::White);
void LogWarning(const std::string& content);
void LogError(const std::string& content);



}
}

