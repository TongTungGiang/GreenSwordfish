#pragma once

#include "ECS.h"

#include "Data/MapData.h"

namespace GS::MapHelper
{

void RandomiseMapEndPoints(Registry& registry);
void SaveMap(Registry& registry, const std::string& mapName, int targetScore, int playTime);
void LoadMap(Registry& registry, const std::string& mapName);
std::vector<std::string> ListAllMapsInMapDirectory();
void SpawnMapTile(Registry& registry, int x, int y, TileType type, Vector2 direction);
void SpawnBackgroundGrid(Registry& registry, const MapTileArray& map, TileType backgroundType);

}
