#pragma once

#ifdef _WIN32
#	if _DEBUG
#		include "optick.h"
#		if USE_PROFILER
#			define PROFILE_FRAME OPTICK_FRAME
#			define PROFILE_SCOPE OPTICK_EVENT
#		else
#			define PROFILE_FRAME
#			define PROFILE_SCOPE
#		endif
#	else
#		define PROFILE_FRAME(name)
#		define PROFILE_SCOPE(name)
#	endif
#else
#	define PROFILE_FRAME(name)
#	define PROFILE_SCOPE(name)
#endif