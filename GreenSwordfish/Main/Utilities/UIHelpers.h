#pragma once

#include "Settings.h"

namespace GS::UIHelpers
{ 

constexpr uint8_t H_LEFT = 1 << 0;
constexpr uint8_t H_CENTER = 1 << 1;
constexpr uint8_t H_RIGHT = 1 << 2;
constexpr uint8_t H_STRETCH = 1 << 3;

constexpr uint8_t V_TOP = 1 << 4;
constexpr uint8_t V_MIDDLE = 1 << 5;
constexpr uint8_t V_BOTTOM = 1 << 6;
constexpr uint8_t V_STRETCH = 1 << 7;

template<uint8_t anchor>
Vector2 CalculatePosition(Vector2 size, Vector2 offset = Vector2::zero)
{
	static_assert(((anchor & H_STRETCH) == 0) && ((anchor & V_STRETCH) == 0),
		"This function cannot be used for calculating rect transform for stretching elements");

	Vector2 anchorPosition;
	if (anchor & H_LEFT)
	{
		anchorPosition.x = 0;
	}
	else if (anchor & H_CENTER)
	{
		anchorPosition.x = WIDTH / 2;
	}

	else if (anchor & H_RIGHT)
	{
		anchorPosition.x = WIDTH;
	}

	if (anchor & V_TOP)
	{
		anchorPosition.y = 0;
	}
	else if (anchor & V_MIDDLE)
	{
		anchorPosition.y = HEIGHT / 2;
	}
	else if (anchor & V_BOTTOM)
	{
		anchorPosition.y = HEIGHT;
	}

	return anchorPosition - (size / 2) + offset;
}

}


