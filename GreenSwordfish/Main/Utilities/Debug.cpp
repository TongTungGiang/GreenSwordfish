#include "Debug.h"

#ifdef _WIN32
#include <windows.h>
#endif

namespace GS
{
namespace Debug
{

#ifdef _WIN32
#	define RED (FOREGROUND_INTENSITY | FOREGROUND_RED)
#	define GREEN (FOREGROUND_INTENSITY | FOREGROUND_GREEN)
#	define YELLOW (FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN)
#	define BLUE (FOREGROUND_INTENSITY | FOREGROUND_BLUE)
#	define MAGENTA (FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE)
#	define CYAN (FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE)
#	define WHITE (FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE)

typedef WORD DebugConsoleColorType;
#else
// On Linux, use ANSI escape sequence for colored text
#	define ESCAPE   "\x1B[0m"

#	define RED      "\x1B[31m"
#	define GREEN    "\x1B[32m"
#	define YELLOW   "\x1B[33m"
#	define BLUE     "\x1B[34m"
#	define MAGENTA  "\x1B[35m"
#	define CYAN     "\x1B[36m"
#	define WHITE    "\x1B[37m"
#	define BOLD(x) "\x1B[1m" << x << RST
#	define UNDERLINE(x) "\x1B[4m" << x << RST

typedef std::string DebugConsoleColorType;
#endif // _WIN32

std::map<DebugTextColor, DebugConsoleColorType> debugColorMap =
{
	{ DebugTextColor::Red, RED },
	{ DebugTextColor::Green, GREEN },
	{ DebugTextColor::Yellow, YELLOW },
	{ DebugTextColor::Blue, BLUE },
	{ DebugTextColor::Magenta, MAGENTA },
	{ DebugTextColor::Cyan, CYAN },
	{ DebugTextColor::White, WHITE }
};

void Log(const std::string& content, DebugTextColor color)
{
#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), debugColorMap[color]);
	std::cout << content << std::endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), debugColorMap[DebugTextColor::White]);
#else
	std::cout << debugColorMap[color] << content << ESCAPE << std::endl;
#endif // _WIN32
}


void LogError(const std::string& content)
{
	Log(content, DebugTextColor::Red);
}


void LogWarning(const std::string& content)
{
	Log(content, DebugTextColor::Yellow);
}


}
}