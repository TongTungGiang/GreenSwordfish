#include "MapHelpers.h"
#include "Data/MapData.h"
#include "Prerequisites.h"

#include "Components/SpriteComponent.h"
#include "Components/TileComponent.h"
#include "Components/ArrowComponent.h"
#include <Components/TextComponent.h>
#include "Components/TransformComponent.h"

#include <filesystem>

namespace GS::MapHelper
{

static constexpr unsigned char NORTH = 0;
static constexpr unsigned char SOUTH = 1;
static constexpr unsigned char EAST = 2;
static constexpr unsigned char WEST = 3;

#define GET_MAP_DIR "Assets/Maps/"
#define GET_MAP_PATH(path) (GET_MAP_DIR + path + ".txt")



void SaveMap(Registry& registry, const std::string& mapName, int targetScore, int playTime)
{
	Debug::Log("Saving map '" + mapName + "' to a json object");
	rapidjson::Document json;

	rapidjson::Value& root = json.SetObject();

	rapidjson::Value jsonMapTiles(rapidjson::kArrayType);
	const MapTileArray& tileMap = registry.ctx<MapTileArray>();
	for (auto it = tileMap.begin(); it != tileMap.end(); ++it)
	{
		int index = it->first;
		MapTileInfo tileInfo = it->second;
		int type = (int)(tileInfo.type);

		unsigned char direction = NORTH;
		if (tileInfo.direction == Vector2::east)
			direction = EAST;
		else if (tileInfo.direction == Vector2::west)
			direction = WEST;
		else if (tileInfo.direction == Vector2::north)
			direction = NORTH;
		else if (tileInfo.direction == Vector2::south)
			direction = SOUTH;

		rapidjson::Value jsonMapTileObject(rapidjson::kObjectType);
		jsonMapTileObject.AddMember("index", index, json.GetAllocator());
		jsonMapTileObject.AddMember("type", type, json.GetAllocator());
		jsonMapTileObject.AddMember("direction", direction, json.GetAllocator());

		jsonMapTiles.PushBack(jsonMapTileObject, json.GetAllocator());
	}

	root.AddMember("map", jsonMapTiles, json.GetAllocator());

	root.AddMember("targetScore", targetScore, json.GetAllocator());
	root.AddMember("playTime", playTime, json.GetAllocator());

	Debug::Log("Done serializing, saving to file");

	std::string fileName = GET_MAP_PATH(mapName);
	std::ofstream outputFileStream;
	outputFileStream.open(fileName, std::fstream::out);
	if (!outputFileStream.is_open())
	{
		Debug::LogError("Couldn't open file name " + fileName + " for writing");
	}
	else
	{
		rapidjson::OStreamWrapper ostreamWrapper(outputFileStream);
		rapidjson::PrettyWriter<rapidjson::OStreamWrapper> writer(ostreamWrapper);
		json.Accept(writer);
		Debug::Log("Done writing to file " + fileName);
	}
}

void LoadMap(Registry& registry, const std::string& mapName)
{
	std::string fileName = GET_MAP_PATH(mapName);
	std::ifstream inputFileStream;
	inputFileStream.open(fileName);
	if (!inputFileStream.is_open())
	{
		Debug::LogError("Couldn't open file name " + fileName + " for reading");

		return;
	}

	rapidjson::IStreamWrapper istreamWrapper(inputFileStream);
	rapidjson::Document json;
	json.ParseStream(istreamWrapper);

	MapTileArray mapData;
	auto root = json.GetObject();
	auto tileList = root["map"].GetArray();
	for (rapidjson::Value::ValueIterator it = tileList.Begin(); it != tileList.End(); ++it)
	{
		MapTileInfo tileInfo;
		tileInfo.isDirty = true;

		auto currentTile = it->GetObject();
		int index = currentTile["index"].GetInt();

		tileInfo.type = (TileType)(currentTile["type"].GetInt());
		unsigned char directionRaw = (unsigned char)currentTile["direction"].GetInt();

		switch (directionRaw)
		{
		case NORTH:
			tileInfo.direction = Vector2::north;
			break;
		case SOUTH:
			tileInfo.direction = Vector2::south;
			break;
		case EAST:
			tileInfo.direction = Vector2::east;
			break;
		case WEST:
			tileInfo.direction = Vector2::west;
			break;
		default:
			break;
		}
		mapData[index] = tileInfo;
	}

	registry.set<MapTileArray>(mapData);

	MapGameplayInfo mapGameplayInfo;
	mapGameplayInfo.playTime = root["playTime"].GetInt();
	mapGameplayInfo.targetScore = root["targetScore"].GetInt();

	registry.set<MapGameplayInfo>(mapGameplayInfo);
}

void InitBlankMap(Registry& registry)
{
	constexpr int maxIndexX = DEFAULT_MAP_SIZE - 1;
	constexpr int minIndexX = 0;
	constexpr int maxIndexY = DEFAULT_MAP_SIZE / 2 - 1;
	constexpr int minIndexY = -DEFAULT_MAP_SIZE / 2;
	for (int x = minIndexX; x <= maxIndexX; x++)
	{
		for (int y = minIndexY; y <= maxIndexY; y++)
		{
			SpawnMapTile(registry, x, y, TileType::Blank, Vector2::zero);
		}
	}
}

void SpawnBackgroundGrid(Registry& registry, const MapTileArray& map, TileType backgroundType)
{
	constexpr int maxIndexX = DEFAULT_MAP_SIZE - 1;
	constexpr int minIndexX = 0;
	constexpr int maxIndexY = DEFAULT_MAP_SIZE / 2 - 1;
	constexpr int minIndexY = -DEFAULT_MAP_SIZE / 2;
	for (int x = minIndexX; x <= maxIndexX; x++)
	{
		for (int y = minIndexY; y <= maxIndexY; y++)
		{
			int index = GetTileIndex(x, y);
			if (IsTileCoordinateValid(map, x, y))
				continue;

			Vector2 world = Math::TileCoordinateToWorld(x, y);
			Vector2 iso = Math::WorldToIso(world);
			Vector2 tileSize = Vector2(TILE_SPRITE_WIDTH_BLANK, TILE_SPRITE_HEIGHT_BLANK);

			auto tileEntity = registry.create();
			SpriteComponent tileSprite(Rect(iso, tileSize));
			tileSprite.SetTexture(g_TileSprites[backgroundType]);
			registry.emplace<SpriteComponent>(tileEntity, tileSprite);
		}
	}
}

void SpawnMapTile(Registry& registry, int x, int y, TileType type, Vector2 direction)
{
	Vector2 world = Math::TileCoordinateToWorld(x, y);
	Vector2 iso = Math::WorldToIso(world);
	Vector2 tileSize = type == TileType::Blank ?
		Vector2(TILE_SPRITE_WIDTH_BLANK, TILE_SPRITE_HEIGHT_BLANK) :
		Vector2(TILE_SPRITE_WIDTH, TILE_SPRITE_HEIGHT);
	Vector2 arrowSize = Vector2(TILE_SPRITE_WIDTH_BLANK, TILE_SPRITE_HEIGHT_BLANK);

	TileComponent tile(x, y);

	auto tileArrowSpriteEntity = registry.create();
	registry.emplace<TileComponent>(tileArrowSpriteEntity, tile);
	registry.emplace<SpriteComponent>(tileArrowSpriteEntity, SpriteComponent(Rect(iso, arrowSize)));
	registry.emplace<ArrowComponent>(tileArrowSpriteEntity, ArrowComponent(direction));
	registry.emplace<TransformComponent>(tileArrowSpriteEntity, TransformComponent(Vector2::zero, world, 1, Layers::GROUND));

	auto tileEntity = registry.create();
	registry.emplace<TileTypeComponent>(tileEntity, TileTypeComponent());
	SpriteComponent tileSprite(Rect(iso, tileSize));
	tileSprite.SetTexture(g_TileSprites[type]);
	registry.emplace<SpriteComponent>(tileEntity, tileSprite);
	registry.emplace<TileComponent>(tileEntity, tile);
	if (type >= TileType::Building1 && type <= TileType::Building4)
		registry.emplace<TransformComponent>(tileEntity, TransformComponent(Vector2::zero, world, 5, Layers::GROUND));
	else
		registry.emplace<TransformComponent>(tileEntity, TransformComponent(Vector2::zero, world, 0, Layers::GROUND));


#if DEBUG_MAP_COORDINATE
	std::string coordinate = std::to_string(x) + "," + std::to_string(y);
	registry.emplace<TextComponent>(tileEntity,
		TextComponent(
			coordinate,
			Rect(iso + Vector2(TILE_SPRITE_WIDTH_BLANK / 4, TILE_SPRITE_HEIGHT_BLANK / 4), tileSize / 2),
			Color::black
		));
#endif
}

void RandomiseMapEndPoints(Registry& registry)
{
	MapTileArray& mapData = registry.ctx<MapTileArray>();
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> randomIndexDistribution((int)TileType::Building1, (int)TileType::Building4);

	for (auto it = mapData.begin(); it != mapData.end(); it++)
	{
		int currentIndex = it->first;
		int tileType = (int)mapData[currentIndex].type;
		if (tileType >= (int)TileType::Building1 && tileType <= (int)TileType::Building4)
		{
			int randomBuildingType = randomIndexDistribution(gen);
			mapData[currentIndex].type = (TileType)randomBuildingType;
		}
	}
}

std::vector<std::string> ListAllMapsInMapDirectory()
{
	std::vector<std::string> result;
	std::filesystem::path mapDir(GET_MAP_DIR);
	std::filesystem::directory_iterator start(mapDir);
	std::filesystem::directory_iterator end;
	for (auto dirIt = start; dirIt != end; dirIt++)
	{
		std::string file = dirIt->path().string();
		size_t lastSlash = file.find_last_of('/');
		file = file.substr(lastSlash + 1, file.length() - lastSlash - 5);
		result.push_back(file);
		Debug::Log("Found map: " + file);
	}
	return result;
}

}