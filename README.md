# Green Swordfish

## Overview
A 2D isometric game using ECS architecture with [EnTT](https://github.com/skypjack/entt/). Rendering is done with SDL.

Project code name is generated randomly and has no special meanings.

## Build instruction

### Windows

#### Requirements
- Windows 10
- Visual Studio and MSVC that support C++17

(Dependencies are included)

#### Build steps
- Clone the repository
- Open GreenSwordfish.sln
- Build the desired configuration. Available platforms are x86 and x64, and available configurations are Debug and Release.

### Linux (experimental)

#### Requirements
- cmake
- libsdl2-dev, libsdl2-image-dev, libsdl2-ttf-dev
- Ubuntu 20.04

(or equivalent in other Linux distros)

#### Build steps
```
$ git clone < repository URL >
$ cd GreenSwordfish
$ mkdir build && cd build
$ cmake ..
$ make
```
